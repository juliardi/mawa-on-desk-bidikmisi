<?php

/**
 * This is the model class for table "tbl_observasi_siswa".
 *
 * The followings are the available columns in table 'tbl_observasi_siswa':
 * @property integer $id_observasi
 * @property integer $tgl_pengumpulan
 * @property string $no_surat_tugas
 * @property integer $tgl_penyerahan_berkas
 * @property integer $tgl_observasi
 * @property string $tempat_observasi
 * @property integer $id_user
 * @property string $keterangan
 * @property string $scan_surat_tugas
 * @property integer $id_tahun_aktif
 *
 * The followings are the available model relations:
 * @property Siswa[] $siswas
 * @property User $idUser
 * @property TahunAktif $idTahunAktif
 */
class ObservasiSiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_observasi_siswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tahun_aktif', 'required'),
			array('tgl_pengumpulan, tgl_penyerahan_berkas, tgl_observasi, id_user, id_tahun_aktif', 'numerical', 'integerOnly'=>true),
			array('no_surat_tugas, scan_surat_tugas', 'length', 'max'=>50),
			array('tempat_observasi', 'length', 'max'=>30),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_observasi, tgl_pengumpulan, no_surat_tugas, tgl_penyerahan_berkas, tgl_observasi, tempat_observasi, id_user, keterangan, scan_surat_tugas, id_tahun_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'siswas' => array(self::HAS_MANY, 'Siswa', 'id_observasi'),
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
			'idTahunAktif' => array(self::BELONGS_TO, 'TahunAktif', 'id_tahun_aktif'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_observasi' => 'Id Observasi',
			'tgl_pengumpulan' => 'Tgl Pengumpulan',
			'no_surat_tugas' => 'No Surat Tugas',
			'tgl_penyerahan_berkas' => 'Tgl Penyerahan Berkas',
			'tgl_observasi' => 'Tgl Observasi',
			'tempat_observasi' => 'Tempat Observasi',
			'id_user' => 'Id User',
			'keterangan' => 'Keterangan',
			'scan_surat_tugas' => 'Scan Surat Tugas',
			'id_tahun_aktif' => 'Id Tahun Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_observasi',$this->id_observasi);
		$criteria->compare('tgl_pengumpulan',$this->tgl_pengumpulan);
		$criteria->compare('no_surat_tugas',$this->no_surat_tugas,true);
		$criteria->compare('tgl_penyerahan_berkas',$this->tgl_penyerahan_berkas);
		$criteria->compare('tgl_observasi',$this->tgl_observasi);
		$criteria->compare('tempat_observasi',$this->tempat_observasi,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('scan_surat_tugas',$this->scan_surat_tugas,true);
		$criteria->compare('id_tahun_aktif',$this->id_tahun_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ObservasiSiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
