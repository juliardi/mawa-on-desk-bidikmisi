<?php

/**
 * Description of ModPersyaratan
 *
 * @author juliardi
 */
class ModPersyaratan extends Persyaratan {
    
    const STATUS_AKTIF = true;
    const STATUS_NON_AKTIF = false;
    
    public function search() {
        $dataProvider = parent::search();
        $dataProvider->pagination->pageSize = 20;
        $dataProvider->criteria->order = 'id_persyaratan ASC';
        return $dataProvider;
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function countSyaratAktif() {
        return (int) self::model()->countByAttributes(array(
            'status' => self::STATUS_AKTIF,
        ));
    }
    
}
