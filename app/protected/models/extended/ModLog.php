<?php

/**
 * Description of ModLog
 *
 * @author juliardi
 */
class ModLog extends Log {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    protected function afterFind() {
        
        $this->waktu = date('H:i:s d M Y', $this->waktu);
        
        return parent::afterFind();
    }
    
    protected function beforeValidate() {
        
        if(!is_integer($this->waktu)) {
            $this->waktu = strtotime($this->waktu);
        }
        
        return parent::beforeValidate();
    }
    
    /**
     * Method untuk menulis log ke database
     * @param integer $id_user
     * @param string $activity
     * @param string $keterangan
     * 
     */
    public static function writeLog($id_user, $activity, $keterangan) {
        $model = new ModLog();
        
        $model->attributes = array(
            'id_user' => $id_user,
            'waktu' => date('Y-m-d H:i:s'),
            'aktivitas' => $activity,
            'keterangan' => $keterangan,
        );
        
        $model->save();
        
    }

}
