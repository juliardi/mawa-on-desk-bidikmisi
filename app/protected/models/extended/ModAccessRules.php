<?php

/**
 * Description of ModAccessRules
 *
 * @author juliardi
 */
class ModAccessRules extends AccessRules {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getAccessRules($id_role, $controller) {
        $models = self::model()->findAllByAttributes(array(
            'id_role' => $id_role,
            'controller' => $controller,
        ));
        
        return $models;
    }
    
}
