<?php

/**
 * Description of ModJenjang
 *
 * @author juliardi
 */
class ModJenjang extends Jenjang {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getJenjangList() {
        $models = self::model()->findAll();
        
        $jenjang = CHtml::listData($models, 'id_jenjang', 'nama_jenjang');
        
        return $jenjang;
    }
    
}
