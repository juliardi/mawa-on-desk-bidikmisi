<?php

/**
 * @author Juliardi
 */
class ModExport extends Export {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function beforeValidate() {
        $this->tgl_export = strtotime($this->tgl_export);
        return parent::beforeValidate();
    }
    
    public function afterFind() {
        $this->tgl_export = date('d-m-Y');
        return parent::afterFind();
    }
    
}