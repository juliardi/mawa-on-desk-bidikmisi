<?php

/**
 * Extended version of Keluarga Model
 *
 * @author juliardi
 */
class ModKeluarga extends Keluarga {
    
    const STATUS_IBU = 0;
    const STATUS_AYAH = 1;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getStatusString() {
        return ($this->status_hubungan == self::STATUS_AYAH) ? "Ayah" : "Ibu";
    }
    
    public static function deleteAllByKap($kap) {
        $models = self::model()->findAllByAttributes(array(
            'kap' => $kap,
        ));
        $result = true;
        
        foreach ($models as $model) {
            $result = $result && $model->delete();
        }
        
        return $result;
    }
    
}
