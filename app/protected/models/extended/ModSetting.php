<?php

/**
 * Description of ModelSetting
 *
 * @author juliardi
 */
class ModSetting extends Setting {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /**
     * Load setting dengan nama setting tertentu
     * @param type $nama_setting
     * @return type
     */
    public static function loadSetting($nama_setting) {
        $model = self::model()->findByAttributes(array(
            'nama_setting' => $nama_setting,
        ));
        
        return $model;
    }
    
    /**
     * Ambil nilai setting tertentu
     * @param type $nama_setting
     * @return type
     */
    public static function getSettingValue($nama_setting) {
        $model = self::loadSetting($nama_setting);
        
        return (is_null($model)) ? '' : $model->nilai;
    }
    
    /**
     * Set nilai setting. Jika nama_setting tidak ditemukan, maka
     * tidak ada nilai yang akan diset.
     * @param type $nama_setting
     * @param type $value
     */
    public static function setSettingValue($nama_setting, $value) {
        $model = self::loadSetting($nama_setting);
        
        if(!is_null($model)) {
            $model->nilai = $value;
            $model->save();
        }
    }
    
}
