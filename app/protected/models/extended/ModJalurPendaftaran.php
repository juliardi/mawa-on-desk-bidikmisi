<?php

/**
 * Description of ModJalurPendaftaran
 *
 * @author juliardi
 */
class ModJalurPendaftaran extends JalurPendaftaran {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getListJalurPendaftaran() {
        $models = self::model()->findAll();
        
        $list = CHtml::listData($models, 'id_jalur_pendaftaran', 'nama_jalur');
        
        return $list;   
    }
    
    public static function getIdJalurByName($nama_jalur) {
        $model = self::model()->findByAttributes(array(
            'nama_jalur' => $nama_jalur,
        ));
        
        if(!is_null($model)) {
            return $model->id_jalur_pendaftaran;
        }
    }
    
}
