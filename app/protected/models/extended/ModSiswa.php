<?php

/**
 * Extended version of Siswa Model
 *
 * The followings are the available columns in table 'tbl_siswa':
 * @property string $no_pendaftaran
 * @property string $kap
 * @property string $nisn
 * @property string $nama
 * @property string $tempat_lahir
 * @property integer $tgl_lahir
 * @property string $jenis_kelamin
 * @property string $agama
 * @property integer $tgl_daftar
 * @property integer $id_observasi
 * @property integer $id_jalur_pendaftaran
 * @property string $no_berkas
 * @property string $no_hp
 * @property integer $id_prodi
 * @property string $email
 * @property integer $id_tahun_aktif
 * @property boolean $status_ondesk
 * @property integer $kode_sekolah
 * @property boolean $status_observasi
 *
 * The followings are the available model relations:
 * @property Alamat[] $alamats
 * @property Keluarga[] $keluargas
 * @property PersyaratanSiswa[] $persyaratanSiswas
 * @property Rumah[] $rumahs
 * @property JalurPendaftaran $idJalurPendaftaran
 * @property Prodi $idProdi
 * @property TahunAktif $idTahunAktif
 * @property ObservasiSiswa $idObservasi
 * @property Sekolah $kodeSekolah
 * 
 * @author juliardi
 */
class ModSiswa extends Siswa {

    const BELUM_ON_DESK = 0;
    const SUDAH_ON_DESK = 1;
    
    const STATUS_BELUM_OBSERVASI = 0;
    const STATUS_SUDAH_OBSERVASI = 1;

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'keluargas' => array(self::HAS_MANY, 'ModKeluarga', 'kap'),
            'persyaratanSiswas' => array(self::HAS_MANY, 'ModPersyaratanSiswa', 'kap'),
            'idJalurPendaftaran' => array(self::BELONGS_TO, 'ModJalurPendaftaran', 'id_jalur_pendaftaran'),
            'idProdi' => array(self::BELONGS_TO, 'ModProdi', 'id_prodi'),
            'idTahunAktif' => array(self::BELONGS_TO, 'ModTahunAktif', 'id_tahun_aktif'),
            'idObservasi' => array(self::BELONGS_TO, 'ModObservasiSiswa', 'id_observasi'),
            'idAlamat' => array(self::HAS_ONE, 'ModAlamat', 'kap'),
            'idRumah' => array(self::HAS_ONE, 'ModRumah', 'kap'),
            'kodeSekolah' => array(self::BELONGS_TO, 'ModSekolah', 'kode_sekolah'),
        );
    }
    
    public function search() {
        
        $this->beforeSearch();
        $dataProvider = parent::search();
        $dataProvider->criteria->order = 'status_ondesk ASC, kap ASC';
        
        return $dataProvider;
    }

    public function customSearch($id_observasi) {
        
        $this->beforeSearch();
        
        $criteria = new CDbCriteria;

        $criteria->addCondition("id_observasi=" . $id_observasi . " OR id_observasi IS NULL");
        $criteria->compare('kap', $this->kap, true);
        $criteria->compare('nisn', $this->nisn, true);
        $criteria->compare('nama', $this->nama, true);
        $criteria->compare('tempat_lahir', $this->tempat_lahir, true);
        $criteria->compare('jenis_kelamin', $this->jenis_kelamin, true);
        $criteria->compare('agama', $this->agama, true);
        $criteria->compare('id_tahun_aktif',$this->id_tahun_aktif);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function afterFind() {
        
        $this->tgl_daftar = date('d-m-Y', $this->tgl_daftar);
        $this->tgl_lahir = date('d-m-Y', $this->tgl_lahir);
        
        return parent::afterFind();
    }
    
    public function beforeValidate() {
        
        $this->tgl_daftar = strtotime($this->tgl_daftar);
        $this->tgl_lahir = strtotime($this->tgl_lahir);
        if($this->isNewRecord) {
            $this->no_berkas = $this->getKodeBerkas($this->no_berkas);
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSearch() {
        
        $this->tgl_daftar = strtotime($this->tgl_daftar);
        $this->tgl_lahir = strtotime($this->tgl_lahir);
        
    }
    
    private function getKodeBerkas($no_berkas) {
        switch ($this->id_jalur_pendaftaran) {
            case 1 : { // Jalur pendaftaran SN
                    return "SN-" . $no_berkas;
                }
            case 2 : { // Jalur pendaftaran SB
                    return "SB-" . $no_berkas;
                }
            case 3 : { // Jalur pendaftaran Mandiri
                    return "M-" . $no_berkas;
                }
        }
    }
    
    public static function isSiswaExist($kap) {
        $model = self::model()->findByPk($kap);
        
        return !is_null($model);
    }
    
    public static function hitungSiswaBelumObservasi($id_observasi) {
        $count = self::model()->countByAttributes(array(
            'id_observasi' => $id_observasi,
            'status_observasi' => false,
        ));
        
        return $count;
    }

}
