<?php

/**
 * Extended version of DataImport Model
 *
 * @author juliardi
 */
class ModDataImport extends DataImport {

    const STATUS_BELUM_IMPORT = 0;
    const STATUS_SUDAH_IMPORT = 1;
    const STATUS_GAGAL_IMPORT = 2;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_import_data', $this->id_import_data);
        $criteria->compare('kap', $this->kap, true);
        $criteria->compare('id_import', $this->id_import);
        $criteria->compare('status', $this->status);
        $criteria->compare('id_prodi', $this->id_prodi);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getStatusName($status) {
        switch ($status) {
            case self::STATUS_BELUM_IMPORT : {
                    return "Belum Import";
                }
            case self::STATUS_SUDAH_IMPORT : {
                    return "Berhasil Import";
                }
            case self::STATUS_GAGAL_IMPORT : {
                    return "Gagal Import";
                }
        }
    }

    public static function isFileImported($id_import) {
        $count = self::model()->countByAttributes(array(
            'id_import' => $id_import,
        ));

        return ($count != 0);
    }

}
