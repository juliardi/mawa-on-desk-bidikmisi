<?php

/**
 * Extended version of TahunAktif Model
 *
 * @author juliardi
 */
class ModTahunAktif extends TahunAktif {
    
    const STATUS_NON_AKTIF = 0;
    const STATUS_AKTIF = 1;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getIdTahunAktif() {
        $model = self::model()->findByAttributes(array(
            'status' => true,
        ));
        
        if(is_null($model)) {
            return '';
        } else {
            return $model->id_tahun_aktif;
        }
    }
    
    public static function getTahunAktif() {
        $model = self::model()->findByAttributes(array(
            'status' => true,
        ));
        
        if(is_null($model)) {
            return '';
        } else {
            return $model->tahun;
        }
    }
    
    public static function getListTahun() {
        $models = self::model()->findAll();
        
        $list = CHtml::listData($models, 'id_tahun_aktif', 'tahun');
        
        return $list;
    }
    
    public function isTahunUsed() {
        return (count($this->siswas) != 0);
    }
    
}
