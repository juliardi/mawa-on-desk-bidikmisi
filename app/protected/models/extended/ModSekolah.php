<?php

/**
 * Description of ModSekolah
 *
 * @author juliardi
 */
class ModSekolah extends Sekolah {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function isSekolahExist($npsn) {
        $model = self::model()->findByPk($npsn);
        
        return !is_null($model);
    }
    
}
