<?php

/**
 * Description of ModProdi
 *
 * @author juliardi
 */
class ModProdi extends Prodi {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getListProdi() {
        $models = self::model()->findAll();
        
        $list = CHtml::listData($models, 'id_prodi', 'nama_prodi');
        
        return $list;
    }
    
    public function isSiswaEmpty() {
        return (count($this->siswas) == 0);
    }
    
    public function isDataImportEmpty() {
        return (count($this->dataImports) == 0);
    }
    
}
