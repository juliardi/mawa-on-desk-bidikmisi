<?php

/**
 * Description of ModRole
 *
 * @author juliardi
 */
class ModRole extends Role {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getIdRoleByName($roleName) {
        $model = self::model()->findByAttributes(array(
            'nama_role' => $roleName,
        ));

        return (is_null($model)) ? null : $model->id_role;
    }
    
    public static function getListRole() {
        $roleSuperadmin = self::getIdRoleByName('superadmin');
        $models = self::model()->findAll('id_role!='.$roleSuperadmin);
        
        $roles = CHtml::listData($models, 'id_role', 'nama_role');
        
        return $roles;
    }
    
}
