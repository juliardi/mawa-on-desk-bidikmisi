<?php

/**
 * Extended version of ObservasiSiswa Model
 *
 * @author juliardi
 */
class ModObservasiSiswa extends ObservasiSiswa {
    
    private $filepath;
    private $filename;
    private $oldScan;
    
    public function rules() {
        $rules = parent::rules();
        $uploadRules = array('scan_surat_tugas', 'uploadFile');
        
        array_push($rules, $uploadRules);
        
        return $rules;
    }
    
    public function uploadFile() {
        if (!is_null($this->scan_surat_tugas) && ($this->scan_surat_tugas instanceof CUploadedFile)) {
            if(!$this->cekEkstensiFile()) {
                $this->addError('scan_surat_tugas', 'Ekstensi file salah. '
                        . 'Hanya boleh mengupload file berekstensi JPG, JPEG, PNG, BMP, atau GIF');
                return;
            }
            
            $this->filename = $this->generateFilename();
            $this->filepath = Yii::app()->params['imagesDir'] . $this->filename;
            if(!$this->scan_surat_tugas->saveAs($this->filepath)) {
                $this->addError('scan_surat_tugas', 'Gagal mengupload gambar');
            }
        } else {
            $this->scan_surat_tugas = $this->oldScan;
        }
    }
    
    private function cekEkstensiFile() {
        $ekstensi = $this->scan_surat_tugas->getExtensionName();
        $listExt = $this->allowedExt();
        
        return $this->isValueExistInArray($ekstensi, $listExt);
    }
    
    private function allowedExt() {
        return array(
            'jpg', 'jpeg', 'png', 'bmp', 'gif',
        );
    }
    
    private function isValueExistInArray($value, $array) {
        foreach ($array as $val) {
            if($val == $value) {
                return true;
            }
        }
        return false;
    }
    
     /**
     * Method untuk menggenerate nama file yang diupload
     * @return String Nama file yang disimpan di server
     */
    private function generateFilename() {
        $ext = $this->scan_surat_tugas->getExtensionName();

        return 'scan-' . $this->id_user . '.' . $ext;
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    protected function beforeValidate() {
        
        $this->tglToInteger();
        
        return parent::beforeValidate();
    }
    
    protected function afterValidate() {
        
        if(!empty($this->filename)) {
            $this->scan_surat_tugas = $this->filename;
        }
        
        if(count($this->errors) > 0) {
            $this->integerToTgl();
        }
        
        return parent::afterValidate();
    }
    
    protected function afterFind() {
        
        $this->integerToTgl();
        $this->oldScan = $this->scan_surat_tugas;
        
        return parent::afterFind();
        
    }
    
    protected function tglToInteger() {
        $this->tgl_observasi = empty($this->tgl_observasi) ? 0 : strtotime($this->tgl_observasi);
        $this->tgl_pengumpulan = empty($this->tgl_pengumpulan) ? 0 : strtotime($this->tgl_pengumpulan);
        $this->tgl_penyerahan_berkas = empty($this->tgl_penyerahan_berkas) ? 0 : strtotime($this->tgl_penyerahan_berkas);
    }
    
    protected function integerToTgl() {
        $this->tgl_penyerahan_berkas = ($this->tgl_penyerahan_berkas == 0) ? '' : date('d-m-Y', $this->tgl_penyerahan_berkas);
        $this->tgl_pengumpulan = ($this->tgl_pengumpulan == 0) ? '' : date('d-m-Y', $this->tgl_pengumpulan);
        $this->tgl_observasi = ($this->tgl_observasi == 0) ? '' : date('d-m-Y',$this->tgl_observasi);
    }
    
}
