<?php

/**
 * Extended version of User Model
 *
 * @author juliardi
 */
class ModUser extends User {

    const ROLE_ADMIN = 1;
    const ROLE_OPERATOR = 2;
    const ROLE_PETUGAS = 3;
    const STATUS_AKTIF = true;
    const STATUS_NON_AKTIF = false;

    public $oldPassword;

    public function rules() {
        $rules = parent::rules();
        $addRule = array('username', 'validateUsername', 'on' => 'insert');

        array_push($rules, $addRule);

        return $rules;
    }

    public function validateUsername() {
        $model = self::model()->findByAttributes(array('username' => $this->username));

        if (!is_null($model)) {
            $this->addError('username', 'Username already exist');
        } else {
            return true;
        }
    }

    public function customSearch() {
        $roleSuperadmin = ModRole::getIdRoleByName('superadmin');
        $criteria = new CDbCriteria;

        $criteria->compare('id_user', $this->id_user);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('id_role', $this->id_role);
        $criteria->addCondition('id_role!='.$roleSuperadmin);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    protected function afterFind() {

        $this->oldPassword = $this->password;

        return parent::afterFind();
    }

    protected function beforeValidate() {

        if ($this->isNewRecord || ($this->password != $this->oldPassword)) {
            $this->salt = HashHelper::generateSalt();
            $this->password = HashHelper::hash($this->password, $this->salt);
        } else {
            $this->password = $this->oldPassword;
        }

        return parent::beforeSave();
    }

    public function getRoleName() {
        return $this->idRole->nama_role;
    }

    public static function getModelByUsername($username) {
        $model = self::model()->findByAttributes(array(
            'username' => $username,
        ));

        return $model;
    }

    public static function getUserActiveByUsername($username) {
        $model = self::model()->findByAttributes(array(
            'username' => $username,
            'status' => self::STATUS_AKTIF,
        ));

        return $model;
    }

    public static function isUserExist($username) {
        return !is_null(self::getModelByUsername($username));
    }

}
