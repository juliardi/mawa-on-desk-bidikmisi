<?php

/**
 * Description of ModPersyaratanSiswa
 *
 * @author juliardi
 */
class ModPersyaratanSiswa extends PersyaratanSiswa {
    
    const STATUS_TIDAK_ADA = 0;
    const STATUS_ADA = 1;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function customSearch() {
        $criteria = new CDbCriteria;

        $criteria->compare('kap', $this->kap);
        $criteria->order = 'id_persyaratan ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }
    
    protected function beforeValidate() {
        
        if($this->status == false) {
            $this->status = 0;
        }
        
        if($this->status == true) {
            $this->status = 1;
        }
        
        return parent::beforeValidate();
    }
    
    public static function countSyaratSiswa($kap) {
        return (int) self::model()->countByAttributes(array('kap' => $kap));
    }
    
    public static function deleteAllByKap($kap) {
        $models = self::model()->findAllByAttributes(array(
            'kap' => $kap,
        ));
        $result = true;
        
        foreach ($models as $model) {
            $result = $result && $model->delete();
        }
        
        return $result;
    }
    
    public static function countSyaratSiswaStatusTrue($kap) {
        $result = self::model()->countByAttributes(array(
            'kap' => $kap,
            'status' => true,
        ));
        
        return $result;
    }
    
    public static function isSyaratSiswaExist($kap, $id_persyaratan) {
        
        $model = self::getModelByKapPersyaratan($kap, $id_persyaratan);
        
        return !is_null($model);
    }
    
    public static function getModelByKapPersyaratan($kap, $id_persyaratan) {
        $model = self::model()->findByAttributes(array(
            'kap' => $kap,
            'id_persyaratan' => $id_persyaratan,
        ));
        
        return $model;
    }
}
