<?php

/**
 * Extended version of Import Model
 *
 * @author juliardi
 */
class ModImport extends Import {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    protected function afterFind() {

        $this->tgl_import = date('d-m-Y', $this->tgl_import);

        return parent::afterFind();
    }

    protected function beforeValidate() {

        $this->tgl_import = strtotime($this->tgl_import);
        if (is_null($this->jumlah_data)) {
            $this->jumlah_data = 0;
        }

        return parent::beforeValidate();
    }

}
