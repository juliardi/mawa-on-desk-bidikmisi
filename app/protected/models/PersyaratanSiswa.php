<?php

/**
 * This is the model class for table "tbl_persyaratan_siswa".
 *
 * The followings are the available columns in table 'tbl_persyaratan_siswa':
 * @property integer $id_ps
 * @property integer $id_persyaratan
 * @property string $kap
 * @property boolean $status
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property Persyaratan $idPersyaratan
 * @property Siswa $kap0
 */
class PersyaratanSiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_persyaratan_siswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_persyaratan, kap, status', 'required'),
			array('id_persyaratan', 'numerical', 'integerOnly'=>true),
			array('kap', 'length', 'max'=>12),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ps, id_persyaratan, kap, status, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPersyaratan' => array(self::BELONGS_TO, 'Persyaratan', 'id_persyaratan'),
			'kap0' => array(self::BELONGS_TO, 'Siswa', 'kap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ps' => 'Id Ps',
			'id_persyaratan' => 'Id Persyaratan',
			'kap' => 'Kap',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ps',$this->id_ps);
		$criteria->compare('id_persyaratan',$this->id_persyaratan);
		$criteria->compare('kap',$this->kap,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersyaratanSiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
