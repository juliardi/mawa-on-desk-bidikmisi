<?php

/**
 * This is the model class for table "tbl_import".
 *
 * The followings are the available columns in table 'tbl_import':
 * @property integer $id_import
 * @property string $nama_file
 * @property integer $tgl_import
 * @property integer $jumlah_data
 * @property integer $id_user
 * @property integer $id_jalur_pendaftaran
 * @property integer $id_tahun_aktif
 *
 * The followings are the available model relations:
 * @property DataImport[] $dataImports
 * @property JalurPendaftaran $idJalurPendaftaran
 * @property TahunAktif $idTahunAktif
 * @property User $idUser
 */
class Import extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_import';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_file, tgl_import, jumlah_data, id_user, id_jalur_pendaftaran', 'required'),
			array('tgl_import, jumlah_data, id_user, id_jalur_pendaftaran, id_tahun_aktif', 'numerical', 'integerOnly'=>true),
			array('nama_file', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_import, nama_file, tgl_import, jumlah_data, id_user, id_jalur_pendaftaran, id_tahun_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dataImports' => array(self::HAS_MANY, 'DataImport', 'id_import'),
			'idJalurPendaftaran' => array(self::BELONGS_TO, 'JalurPendaftaran', 'id_jalur_pendaftaran'),
			'idTahunAktif' => array(self::BELONGS_TO, 'TahunAktif', 'id_tahun_aktif'),
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_import' => 'Id Import',
			'nama_file' => 'Nama File',
			'tgl_import' => 'Tgl Import',
			'jumlah_data' => 'Jumlah Data',
			'id_user' => 'Id User',
			'id_jalur_pendaftaran' => 'Id Jalur Pendaftaran',
			'id_tahun_aktif' => 'Id Tahun Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_import',$this->id_import);
		$criteria->compare('nama_file',$this->nama_file,true);
		$criteria->compare('tgl_import',$this->tgl_import);
		$criteria->compare('jumlah_data',$this->jumlah_data);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_jalur_pendaftaran',$this->id_jalur_pendaftaran);
		$criteria->compare('id_tahun_aktif',$this->id_tahun_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Import the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
