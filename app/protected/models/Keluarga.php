<?php

/**
 * This is the model class for table "tbl_keluarga".
 *
 * The followings are the available columns in table 'tbl_keluarga':
 * @property integer $id_keluarga
 * @property string $nama
 * @property string $pendidikan
 * @property string $pekerjaan
 * @property string $penghasilan
 * @property string $status
 * @property integer $status_hubungan
 * @property string $kap
 *
 * The followings are the available model relations:
 * @property Siswa $kap0
 */
class Keluarga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_keluarga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, pendidikan, pekerjaan, penghasilan, status, status_hubungan, kap', 'required'),
			array('status_hubungan', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>100),
			array('pendidikan, pekerjaan, penghasilan', 'length', 'max'=>50),
			array('status', 'length', 'max'=>10),
			array('kap', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_keluarga, nama, pendidikan, pekerjaan, penghasilan, status, status_hubungan, kap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kap0' => array(self::BELONGS_TO, 'Siswa', 'kap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_keluarga' => 'Id Keluarga',
			'nama' => 'Nama',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
			'penghasilan' => 'Penghasilan',
			'status' => 'Status',
			'status_hubungan' => 'Status Hubungan',
			'kap' => 'Kap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_keluarga',$this->id_keluarga);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('penghasilan',$this->penghasilan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_hubungan',$this->status_hubungan);
		$criteria->compare('kap',$this->kap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Keluarga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
