<?php

/**
 * This is the model class for table "tbl_alamat".
 *
 * The followings are the available columns in table 'tbl_alamat':
 * @property integer $id_alamat
 * @property string $alamat
 * @property string $kota
 * @property string $provinsi
 * @property string $kap
 *
 * The followings are the available model relations:
 * @property Siswa $kap0
 */
class Alamat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_alamat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alamat', 'length', 'max'=>255),
			array('kota', 'length', 'max'=>100),
			array('provinsi', 'length', 'max'=>30),
			array('kap', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_alamat, alamat, kota, provinsi, kap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kap0' => array(self::BELONGS_TO, 'Siswa', 'kap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_alamat' => 'Id Alamat',
			'alamat' => 'Alamat',
			'kota' => 'Kota',
			'provinsi' => 'Provinsi',
			'kap' => 'Kap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_alamat',$this->id_alamat);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('provinsi',$this->provinsi,true);
		$criteria->compare('kap',$this->kap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Alamat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
