<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id_user
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property integer $id_role
 * @property boolean $status
 * @property string $nama
 * @property string $no_hp
 *
 * The followings are the available model relations:
 * @property Import[] $imports
 * @property Role $idRole
 * @property Log[] $logs
 * @property Export[] $exports
 * @property ObservasiSiswa[] $observasiSiswas
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, salt, id_role', 'required'),
			array('id_role', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>30),
			array('password, salt', 'length', 'max'=>128),
			array('nama', 'length', 'max'=>50),
			array('no_hp', 'length', 'max'=>12),
			array('status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_user, username, password, salt, id_role, status, nama, no_hp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imports' => array(self::HAS_MANY, 'Import', 'id_user'),
			'idRole' => array(self::BELONGS_TO, 'Role', 'id_role'),
			'logs' => array(self::HAS_MANY, 'Log', 'id_user'),
			'exports' => array(self::HAS_MANY, 'Export', 'id_user'),
			'observasiSiswas' => array(self::HAS_MANY, 'ObservasiSiswa', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'username' => 'Username',
			'password' => 'Password',
			'salt' => 'Salt',
			'id_role' => 'Id Role',
			'status' => 'Status',
			'nama' => 'Nama',
			'no_hp' => 'No Hp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('id_role',$this->id_role);
		$criteria->compare('status',$this->status);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('no_hp',$this->no_hp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
