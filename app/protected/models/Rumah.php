<?php

/**
 * This is the model class for table "tbl_rumah".
 *
 * The followings are the available columns in table 'tbl_rumah':
 * @property integer $id_rumah
 * @property string $kepemilikan
 * @property string $luas_tanah
 * @property string $luas_bangunan
 * @property string $listrik
 * @property string $sumber_air
 * @property string $mck
 * @property string $bahan_atap
 * @property string $bahan_lantai
 * @property string $bahan_tembok
 * @property integer $tahun_perolehan
 * @property string $jarak_kekota
 * @property string $kap
 *
 * The followings are the available model relations:
 * @property Siswa $kap0
 */
class Rumah extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_rumah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tahun_perolehan', 'numerical', 'integerOnly'=>true),
			array('kepemilikan, luas_tanah, luas_bangunan, listrik, sumber_air, mck, bahan_atap, bahan_lantai, bahan_tembok', 'length', 'max'=>50),
			array('jarak_kekota', 'length', 'max'=>10),
			array('kap', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rumah, kepemilikan, luas_tanah, luas_bangunan, listrik, sumber_air, mck, bahan_atap, bahan_lantai, bahan_tembok, tahun_perolehan, jarak_kekota, kap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kap0' => array(self::BELONGS_TO, 'Siswa', 'kap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rumah' => 'Id Rumah',
			'kepemilikan' => 'Kepemilikan',
			'luas_tanah' => 'Luas Tanah',
			'luas_bangunan' => 'Luas Bangunan',
			'listrik' => 'Listrik',
			'sumber_air' => 'Sumber Air',
			'mck' => 'Mck',
			'bahan_atap' => 'Bahan Atap',
			'bahan_lantai' => 'Bahan Lantai',
			'bahan_tembok' => 'Bahan Tembok',
			'tahun_perolehan' => 'Tahun Perolehan',
			'jarak_kekota' => 'Jarak Kekota',
			'kap' => 'Kap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rumah',$this->id_rumah);
		$criteria->compare('kepemilikan',$this->kepemilikan,true);
		$criteria->compare('luas_tanah',$this->luas_tanah,true);
		$criteria->compare('luas_bangunan',$this->luas_bangunan,true);
		$criteria->compare('listrik',$this->listrik,true);
		$criteria->compare('sumber_air',$this->sumber_air,true);
		$criteria->compare('mck',$this->mck,true);
		$criteria->compare('bahan_atap',$this->bahan_atap,true);
		$criteria->compare('bahan_lantai',$this->bahan_lantai,true);
		$criteria->compare('bahan_tembok',$this->bahan_tembok,true);
		$criteria->compare('tahun_perolehan',$this->tahun_perolehan);
		$criteria->compare('jarak_kekota',$this->jarak_kekota,true);
		$criteria->compare('kap',$this->kap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rumah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
