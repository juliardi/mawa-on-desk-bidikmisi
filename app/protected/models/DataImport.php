<?php

/**
 * This is the model class for table "tbl_data_import".
 *
 * The followings are the available columns in table 'tbl_data_import':
 * @property integer $id_import_data
 * @property string $kap
 * @property integer $id_import
 * @property integer $status
 * @property integer $id_prodi
 *
 * The followings are the available model relations:
 * @property Prodi $idProdi
 * @property Import $idImport
 */
class DataImport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_data_import';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kap, id_import, status', 'required'),
			array('id_import, status, id_prodi', 'numerical', 'integerOnly'=>true),
			array('kap', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_import_data, kap, id_import, status, id_prodi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProdi' => array(self::BELONGS_TO, 'Prodi', 'id_prodi'),
			'idImport' => array(self::BELONGS_TO, 'Import', 'id_import'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_import_data' => 'Id Import Data',
			'kap' => 'Kap',
			'id_import' => 'Id Import',
			'status' => 'Status',
			'id_prodi' => 'Id Prodi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_import_data',$this->id_import_data);
		$criteria->compare('kap',$this->kap,true);
		$criteria->compare('id_import',$this->id_import);
		$criteria->compare('status',$this->status);
		$criteria->compare('id_prodi',$this->id_prodi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataImport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
