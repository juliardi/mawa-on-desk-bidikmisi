<?php

/**
 * This is the model class for table "tbl_siswa".
 *
 * The followings are the available columns in table 'tbl_siswa':
 * @property string $no_pendaftaran
 * @property string $kap
 * @property string $nisn
 * @property string $nama
 * @property string $tempat_lahir
 * @property integer $tgl_lahir
 * @property string $jenis_kelamin
 * @property string $agama
 * @property integer $tgl_daftar
 * @property integer $id_observasi
 * @property integer $id_jalur_pendaftaran
 * @property string $no_berkas
 * @property string $no_hp
 * @property integer $id_prodi
 * @property string $email
 * @property integer $id_tahun_aktif
 * @property boolean $status_ondesk
 * @property integer $kode_sekolah
 * @property boolean $status_observasi
 *
 * The followings are the available model relations:
 * @property Alamat[] $alamats
 * @property Keluarga[] $keluargas
 * @property PersyaratanSiswa[] $persyaratanSiswas
 * @property Rumah[] $rumahs
 * @property JalurPendaftaran $idJalurPendaftaran
 * @property Prodi $idProdi
 * @property TahunAktif $idTahunAktif
 * @property ObservasiSiswa $idObservasi
 * @property Sekolah $kodeSekolah
 */
class Siswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_siswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kap, nisn, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, tgl_daftar, kode_sekolah', 'required'),
			array('tgl_lahir, tgl_daftar, id_observasi, id_jalur_pendaftaran, id_prodi, id_tahun_aktif, kode_sekolah', 'numerical', 'integerOnly'=>true),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('kap, no_hp', 'length', 'max'=>12),
			array('nisn, agama, no_berkas', 'length', 'max'=>10),
			array('nama, email', 'length', 'max'=>100),
			array('tempat_lahir', 'length', 'max'=>60),
			array('jenis_kelamin', 'length', 'max'=>1),
			array('status_ondesk, status_observasi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_pendaftaran, kap, nisn, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, tgl_daftar, id_observasi, id_jalur_pendaftaran, no_berkas, no_hp, id_prodi, email, id_tahun_aktif, status_ondesk, kode_sekolah, status_observasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alamats' => array(self::HAS_MANY, 'Alamat', 'kap'),
			'keluargas' => array(self::HAS_MANY, 'Keluarga', 'kap'),
			'persyaratanSiswas' => array(self::HAS_MANY, 'PersyaratanSiswa', 'kap'),
			'rumahs' => array(self::HAS_MANY, 'Rumah', 'kap'),
			'idJalurPendaftaran' => array(self::BELONGS_TO, 'JalurPendaftaran', 'id_jalur_pendaftaran'),
			'idProdi' => array(self::BELONGS_TO, 'Prodi', 'id_prodi'),
			'idTahunAktif' => array(self::BELONGS_TO, 'TahunAktif', 'id_tahun_aktif'),
			'idObservasi' => array(self::BELONGS_TO, 'ObservasiSiswa', 'id_observasi'),
			'kodeSekolah' => array(self::BELONGS_TO, 'Sekolah', 'kode_sekolah'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_pendaftaran' => 'No Pendaftaran',
			'kap' => 'Kap',
			'nisn' => 'Nisn',
			'nama' => 'Nama',
			'tempat_lahir' => 'Tempat Lahir',
			'tgl_lahir' => 'Tgl Lahir',
			'jenis_kelamin' => 'Jenis Kelamin',
			'agama' => 'Agama',
			'tgl_daftar' => 'Tgl Daftar',
			'id_observasi' => 'Id Observasi',
			'id_jalur_pendaftaran' => 'Id Jalur Pendaftaran',
			'no_berkas' => 'No Berkas',
			'no_hp' => 'No Hp',
			'id_prodi' => 'Id Prodi',
			'email' => 'Email',
			'id_tahun_aktif' => 'Id Tahun Aktif',
			'status_ondesk' => 'Status Ondesk',
			'kode_sekolah' => 'Kode Sekolah',
			'status_observasi' => 'Status Observasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
		$criteria->compare('kap',$this->kap,true);
		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('tgl_daftar',$this->tgl_daftar);
		$criteria->compare('id_observasi',$this->id_observasi);
		$criteria->compare('id_jalur_pendaftaran',$this->id_jalur_pendaftaran);
		$criteria->compare('no_berkas',$this->no_berkas,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('id_prodi',$this->id_prodi);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_tahun_aktif',$this->id_tahun_aktif);
		$criteria->compare('status_ondesk',$this->status_ondesk);
		$criteria->compare('kode_sekolah',$this->kode_sekolah);
		$criteria->compare('status_observasi',$this->status_observasi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Siswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
