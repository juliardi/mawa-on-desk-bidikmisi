<?php

/**
 * This is the model class for table "tbl_sekolah".
 *
 * The followings are the available columns in table 'tbl_sekolah':
 * @property integer $npsn
 * @property string $nama_sekolah
 * @property string $alamat
 * @property string $kota
 * @property string $provinsi
 *
 * The followings are the available model relations:
 * @property Siswa[] $siswas
 */
class Sekolah extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sekolah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('npsn, nama_sekolah', 'required'),
			array('npsn', 'numerical', 'integerOnly'=>true),
			array('nama_sekolah, kota, provinsi', 'length', 'max'=>50),
			array('alamat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('npsn, nama_sekolah, alamat, kota, provinsi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'siswas' => array(self::HAS_MANY, 'Siswa', 'kode_sekolah'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'npsn' => 'Npsn',
			'nama_sekolah' => 'Nama Sekolah',
			'alamat' => 'Alamat',
			'kota' => 'Kota',
			'provinsi' => 'Provinsi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('npsn',$this->npsn);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('provinsi',$this->provinsi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sekolah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
