<?php

/**
 * This is the model class for table "tbl_prodi".
 *
 * The followings are the available columns in table 'tbl_prodi':
 * @property integer $id_prodi
 * @property string $nama_prodi
 * @property integer $id_jenjang
 * @property string $kode_prodi
 *
 * The followings are the available model relations:
 * @property DataImport[] $dataImports
 * @property Siswa[] $siswas
 * @property Jenjang $idJenjang
 */
class Prodi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_prodi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_prodi, id_jenjang', 'required'),
			array('id_jenjang', 'numerical', 'integerOnly'=>true),
			array('nama_prodi', 'length', 'max'=>100),
			array('kode_prodi', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_prodi, nama_prodi, id_jenjang, kode_prodi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dataImports' => array(self::HAS_MANY, 'DataImport', 'id_prodi'),
			'siswas' => array(self::HAS_MANY, 'Siswa', 'id_prodi'),
			'idJenjang' => array(self::BELONGS_TO, 'Jenjang', 'id_jenjang'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_prodi' => 'Id Prodi',
			'nama_prodi' => 'Nama Prodi',
			'id_jenjang' => 'Id Jenjang',
			'kode_prodi' => 'Kode Prodi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_prodi',$this->id_prodi);
		$criteria->compare('nama_prodi',$this->nama_prodi,true);
		$criteria->compare('id_jenjang',$this->id_jenjang);
		$criteria->compare('kode_prodi',$this->kode_prodi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prodi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
