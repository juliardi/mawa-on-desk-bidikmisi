<?php $this->pageTitle = "Data User"; ?>

<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data User',
    'headerIcon' => 'user',
));

echo $this->renderPartial('__data_user', compact('model'));

$this->endWidget();
