<?php

$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'horizotalForm',
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well col-lg-7'), // for inset effect
        )
);

echo $form->errorSummary($model);
echo $form->textFieldGroup($model, 'username', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'pattern' => '^([a-zA-Z])[A-Za-z0-9._@]{4,30}$',
            'title' => 'Username harus diawali huruf dan hanya boleh menggunakan karakter A-Z, a-z, 0-9, underscore, @, dan titik',
        ),
    )
));

echo $form->passwordFieldGroup($model, 'password');

echo $form->textFieldGroup($model, 'nama');

echo $form->textFieldGroup($model, 'no_hp');

echo $form->dropDownListGroup($model, 'id_role', array(
    'widgetOptions' => array(
        'data' => ModRole::getListRole(),
    ),
));

$this->widget(
        'booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => ucfirst($model->scenario),
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block'
    ),
        )
);

$this->endWidget();
