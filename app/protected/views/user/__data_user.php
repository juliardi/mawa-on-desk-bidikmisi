<?php

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'username',
        array(
            'label' => 'Role',
            'value' => $model->getRoleName(),
        ),
    )
));
