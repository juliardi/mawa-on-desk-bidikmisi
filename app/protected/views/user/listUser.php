<?php

$this->pageTitle = 'List User';

echo CHtml::link('Tambah User', array('user/addUser'), array(
    'class' => 'btn btn-primary',
));

echo '<br>';

$filterRoleUser = $this->renderPartial('__filter_role_user', compact('model'), true);

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->customSearch(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'columns' => array(
        'username',
        array(
            'name' => 'id_role',
            'value' => '$data->getRoleName()',
            'filter' => $filterRoleUser,
        ),
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
        array(
            'header' => 'Status',
            'type' => 'raw',
            'value' => array($this, 'getStatusButton'),
        ),
    ),
        )
);
