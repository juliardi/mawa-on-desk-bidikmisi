<?php $url = Yii::app()->createUrl('rules/deleteRules');
    $script = "return confirm('Hapus Rules ini?')";
?>

<form method="POST" action="<?php echo $url; ?>" onsubmit="<?php echo $script; ?>">
    <input type="hidden" name="id" value="<?php echo $data->id_access_rules; ?>">
    <input type="submit" name="delete" value="Delete" class="btn btn-danger">
</form>