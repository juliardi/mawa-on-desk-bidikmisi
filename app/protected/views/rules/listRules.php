<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#access-rules-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'List Access Rules';

echo CHtml::link('Tambah Rules', array('rules/addRules'), array(
    'class' => 'btn btn-primary',
));

echo '<br>';

$controller = $this;

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'access-rules-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id_role',
            'header' => 'Role',
            'value' => '$data->idRole->nama_role',
        ),
        'controller',
        'action',
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
        array(
            'header' => 'Delete',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__delete_button', compact('data'), true);
            },
        ),
    ),
));