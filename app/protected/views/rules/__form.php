<?php

$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
    )
        ));

echo $form->errorSummary($model);
echo $form->dropDownListGroup($model, 'id_role', array(
    'widgetOptions' => array(
        'data' => ModRole::getListRole(),
    ),
));
echo $form->textFieldGroup($model, 'controller');
echo $form->textFieldGroup($model, 'action');

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => ucfirst($model->scenario),
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block',
    )
));

$this->endWidget();