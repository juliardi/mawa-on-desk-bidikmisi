<?php

$this->pageTitle = 'Login';
?>

<?php

$form = $this->beginWidget('TbActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('class' => 'well col-lg-5'),
        ));

echo $form->textFieldGroup($model, 'username');

echo $form->passwordFieldGroup($model, 'password');

echo CHtml::submitButton('Login', array(
    'class' => 'btn btn-primary',
));

$this->endWidget();
?>

