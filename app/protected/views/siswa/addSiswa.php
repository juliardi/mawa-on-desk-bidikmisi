<?php

$this->pageTitle = "Tambah Siswa";

echo CHtml::link('Kembali', Yii::app()->createUrl('siswa/listSiswa'), array(
    'class' => 'btn btn-default',
));
echo '<br><br>';


$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array('class' => 'well col-lg-7'), // for inset effect
        )
);

echo $form->errorSummary($model);

echo $form->textFieldGroup($model, 'kap', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'required' => true,
            'pattern' => '^([0-9]{12})$',
            'title' => 'Nomor KAP harus terdiri dari 12 angka',
        ),
    )
));

echo $form->textFieldGroup($model, 'no_pendaftaran');

echo $form->hiddenField($model, 'jalur_pendaftaran', array(
    'value' => '3',
));

echo $form->hiddenField($model, 'status_ondesk', array(
    'value' => ModSiswa::BELUM_ON_DESK,
));

echo $form->dropDownListGroup($model, 'id_prodi', array(
    'widgetOptions' => array(
        'data' => ModProdi::getListProdi(),
    ),
));

$this->widget(
        'booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Submit',
    'htmlOptions' => array('class' => 'btn-primary btn-block'),
        )
);

$this->endWidget();

