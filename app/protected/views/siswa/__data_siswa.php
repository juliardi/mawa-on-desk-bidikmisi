<br>
<?php
$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Siswa',
    'headerIcon' => 'user',
));

echo CHtml::link('Edit Data', Yii::app()->createUrl('siswa/editSiswa', array(
    'kap' => $model->kap,
)), array(
    'class' => 'btn btn-primary',
));

echo '<br><br>';

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'kap',
        'nisn',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'agama',
        'no_hp',
        'email',
        array(
            'label' => 'Prodi Diterima',
            'value' => $model->idProdi->nama_prodi,
        ),
        array(
            'label' => 'Jalur Pendaftaran',
            'value' => $model->idJalurPendaftaran->nama_jalur,
        ),
    )
));

$this->endWidget();