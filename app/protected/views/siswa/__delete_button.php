<?php $url = Yii::app()->createUrl('siswa/deleteSiswa');
    $script = "return confirm('Hapus Siswa ini?')";
?>

<form method="POST" action="<?php echo $url; ?>" onsubmit="<?php echo $script; ?>">
    <input type="hidden" name="ModSiswa[kap]" value="<?php echo $data->kap; ?>">
    <input type="submit" name="delete" value="Delete" class="btn btn-danger">
</form>