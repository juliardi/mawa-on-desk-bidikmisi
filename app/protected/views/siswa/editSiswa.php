<?php

echo CHtml::link('Kembali', Yii::app()->createUrl('siswa/showSiswa', array('kap'=>$model->kap)), array(
    'class' => 'btn btn-default',
));
echo '<br><br>';

$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
    )
        ));

echo $form->errorSummary($model);

echo $form->textFieldGroup($model, 'kap', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'disabled' => true,
        ),
    )
));

echo $form->textFieldGroup($model, 'no_pendaftaran', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'disabled' => true,
        ),
    )
));

echo $form->textFieldGroup($model, 'nisn', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'disabled' => true,
        ),
    )
));

echo $form->textFieldGroup($model, 'no_berkas');

echo $form->textFieldGroup($model, 'nama');

echo $form->textFieldGroup($model, 'tempat_lahir');

echo $form->datePickerGroup($model, 'tgl_lahir', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'dd-mm-yyyy',
            'read-only' => 'true',
        )
    ),
));

echo $form->dropDownListGroup($model, 'jenis_kelamin', array(
    'widgetOptions' => array(
        'data' => array(
            'L' => 'Laki-laki',
            'P' => 'Perempuan',
        )
    ),
));

echo $form->dropDownListGroup($model, 'agama', array(
    'widgetOptions' => array(
        'data' => array(
            'Islam' => 'Islam',
            'Kristen' => 'Kristen',
            'Katolik' => 'Katolik',
            'Hindu' => 'Hindu',
            'Budha' => 'Budha',
            'Konghucu' => 'Konghucu',
        )
    ),
));

echo $form->textFieldGroup($model, 'no_hp');

echo $form->emailFieldGroup($model, 'email');

echo $form->dropDownListGroup($model, 'id_jalur_pendaftaran', array(
    'widgetOptions' => array(
        'data' => ModJalurPendaftaran::getListJalurPendaftaran(),
    ),
));

echo $form->dropDownListGroup($model, 'id_prodi', array(
    'widgetOptions' => array(
        'data' => ModProdi::getListProdi(),
    ),
));

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => ucfirst($model->scenario),
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block',
    )
));

$this->endWidget();