<br>
<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Alamat Siswa',
    'headerIcon' => 'home',
));

echo CHtml::link('Edit Data', Yii::app()->createUrl('siswa/editAlamat', array(
    'id' => $model->id_alamat,
)), array(
    'class' => 'btn btn-primary',
));

echo '<br><br>';

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'alamat',
        'kota',
        'provinsi',
    )
));

$this->endWidget();