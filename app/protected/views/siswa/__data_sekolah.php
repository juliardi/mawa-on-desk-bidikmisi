<br>
<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Sekolah Siswa',
    'headerIcon' => 'home',
));

echo CHtml::link('Edit Data', Yii::app()->createUrl('siswa/editSekolah', array(
    'id' => $model->npsn,
)), array(
    'class' => 'btn btn-primary',
));

echo '<br><br>';

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'npsn',
        'nama_sekolah',
        'alamat',
        'kota',
        'provinsi',
    )
));

$this->endWidget();