<?php
echo CHtml::dropDownList('ModSiswa[status_ondesk]', $model->status_ondesk, array(
    '0' => 'Belum On-Desk',
    '1' => 'Sudah On-Desk',
    'empty' => 'VIEW ALL',
    '' => '-------------',
), array(
    'class' => 'form-control',
));
