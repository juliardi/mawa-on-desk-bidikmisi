<?php

$this->pageTitle = 'List Siswa';

$filterOndesk = $this->renderPartial('__filter_siswa_ondesk', compact('model'), true);

echo CHtml::link('Tambah Siswa', Yii::app()->createUrl('siswa/addSiswa'), array(
    'class' => 'btn btn-primary',
));

$controller = $this;

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'kap',
        'no_pendaftaran',
        'no_berkas',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        array(
            'header' => 'Prodi',
            'value' => '$data->idProdi->nama_prodi',
        ),
        array(
            'header' => 'Detail',
            'type' => 'raw',
            'value' => array($this, 'getDetailButton'),
        ),
        array(
            'header' => 'Persyaratan',
            'type' => 'raw',
            'value' => array($this, 'getSyaratButton'),
        ),
        array(
            'name' => 'status_ondesk',
            'type' => 'raw',
            'value' => array($this, 'getStatusOnDesk'),
            'filter' => $filterOndesk,
        ),
        array(
            'header' => 'Delete',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__delete_button', compact('data'), true);
            },
        ),
    ),
        )
);

    $url = Yii::app()->createUrl('siswa/updateOnDesk');
    
    ?>

<script>
    $("input[type=checkbox]").click(function(e) {
        var cb = document.getElementById($(this).attr('id'));

        $.ajax({
            type: "POST",
            url: "<?php echo $url; ?>",
            data: {
                Data: {
                    kap: $(this).attr('id'),
                    status: (cb.checked ? 1 : 0)
                }
            }
        });
    });
</script>