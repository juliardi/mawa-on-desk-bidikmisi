<?php

$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
    )
        ));

echo $form->errorSummary($model);

echo $form->textFieldGroup($model, 'npsn', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'disabled' => true,
        ),
    )
));

echo $form->textFieldGroup($model, 'nama_sekolah');

echo $form->textFieldGroup($model, 'alamat');

echo $form->textFieldGroup($model, 'kota');

echo $form->textFieldGroup($model, 'provinsi');

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Update',
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block',
    )
));

$this->endWidget();