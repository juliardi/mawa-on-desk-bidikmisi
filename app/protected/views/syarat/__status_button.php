<?php $url = Yii::app()->createUrl('syarat/updateStatus'); ?>

<form method="POST" action="<?php echo $url; ?>">
    <input type="hidden" name="ModPersyaratan[id_persyaratan]" value="<?php echo $data->id_persyaratan; ?>">
    <input type="hidden" name="ModPersyaratan[status]" value="<?php echo !$data->status; ?>">
    <input 
        type="submit" 
        value="<?php echo ($data->status) ? 'Non-Aktifkan' : 'Aktifkan' ?>" 
        class="<?php echo ($data->status) ? 'btn btn-danger' : 'btn btn-primary' ?>">
</form>