<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#syarat-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'List Persyaratan';

echo CHtml::link('Tambah Persyaratan', array('syarat/addSyarat'), array(
    'class' => 'btn btn-primary',
));

echo '<br>';

$controller = $this;

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'syarat-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        'syarat',
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
        array(
            'header' => 'Status',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__status_button', compact('data'), true);
            },
        ),
    ),
));