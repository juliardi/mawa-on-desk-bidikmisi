<?php

$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
    )
        ));

echo $form->textAreaGroup($model, 'syarat');

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => ($model->isNewRecord) ? 'Insert' : 'Update',
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block',
    )
));

$this->endWidget();