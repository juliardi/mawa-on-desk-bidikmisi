<?php $this->pageTitle = "Data Siswa";

echo CHtml::link('Kembali', Yii::app()->createUrl('laporan/listSiswa'), array(
    'class' => 'btn btn-default',
));

echo '<br><br>';

$this->widget(
        'booster.widgets.TbTabs', array(
    'type' => 'tabs', // 'tabs' or 'pills'
    'tabs' => array(
        array(
            'label' => 'Data Siswa',
            'content' => $this->renderPartial('__data_siswa', compact('model'), true),
            'active' => true
        ),
        array(
            'label' => 'Sekolah Siswa',
            'content' => $this->renderPartial('__data_sekolah', array('model' => $model->kodeSekolah), true),
        ),
        array(
            'label' => 'Alamat Siswa',
            'content' => $this->renderPartial('__data_alamat', array('model' => $model->idAlamat), true),
        ),
        array(
            'label' => 'Rumah Siswa',
            'content' => $this->renderPartial('__data_rumah', array('model' => $model->idRumah), true),
        ),
        array(
            'label' => 'Keluarga Siswa',
            'content' => $this->renderPartial('__data_keluarga', array('model' => $model->keluargas), true),
        ),
    ),
        )
);
