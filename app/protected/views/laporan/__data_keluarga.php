<br>
<?php

foreach ($model as $keluarga) {

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => $keluarga->getStatusString(),
    'headerIcon' => 'user',
));

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $keluarga,
    'attributes' => array(
        'nama',
        'pendidikan',
        'pekerjaan',
        'penghasilan',
        'status',
    )
));

$this->endWidget();

}