<div class="well col-lg-6">
    <div class="form-group">
        <label class="control-label" for="JumlahSiswa">Jumlah Siswa :</label>
        <label class="control-label" name="JumlahSiswa">
            <?php echo ModSiswa::model()->count(); ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahOnDesk">Jumlah Siswa Sudah On-Desk:</label>
        <label class="control-label" name="JumlahOnDesk">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'status_ondesk' => true,
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahBelumOndesk">Jumlah Siswa Belum On-Desk:</label>
        <label class="control-label" name="JumlahBelumOndesk">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'status_ondesk' => false,
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahSudahObservasi">Jumlah Siswa Sudah Diobservasi:</label>
        <label class="control-label" name="JumlahSudahObservasi">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'status_observasi' => true,
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahBelumObservasi">Jumlah Siswa Belum Diobservasi:</label>
        <label class="control-label" name="JumlahBelumObservasi">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'status_observasi' => false,
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahSN">Jumlah Siswa Jalur SN:</label>
        <label class="control-label" name="JumlahSN">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'id_jalur_pendaftaran' => ModJalurPendaftaran::getIdJalurByName('SN'),
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahSB">Jumlah Siswa Jalur SB:</label>
        <label class="control-label" name="JumlahSB">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'id_jalur_pendaftaran' => ModJalurPendaftaran::getIdJalurByName('SB'),
            ));
            ?>
        </label>
    </div>
    <div class="form-group">
        <label class="control-label" for="JumlahMandiri">Jumlah Siswa Jalur Mandiri:</label>
        <label class="control-label" name="JumlahMandiri">
            <?php
            echo ModSiswa::model()->countByAttributes(array(
                'id_jalur_pendaftaran' => ModJalurPendaftaran::getIdJalurByName('Mandiri'),
            ));
            ?>
        </label>
    </div>
</div>
