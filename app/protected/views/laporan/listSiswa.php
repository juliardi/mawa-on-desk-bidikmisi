<?php
$this->pageTitle = 'List Siswa';

$filterOndesk = $this->renderPartial('__filter_siswa_ondesk', compact('model'), true);
$filterObservasi = $this->renderPartial('__filter_siswa_observasi', compact('model'), true);

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'kap',
        'nisn',
        'no_berkas',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        array(
            'header' => 'Detail',
            'type' => 'raw',
            'value' => array($this, 'getDetailSiswaButton'),
        ),
        array(
            'name' => 'status_ondesk',
            'type' => 'raw',
            'value' => array($this, 'getStatusOndesk'),
            'filter' => $filterOndesk,
        ),
        array(
            'name' => 'status_observasi',
            'type' => 'raw',
            'value' => array($this, 'getStatusObservasi'),
            'filter' => $filterObservasi,
        ),
    ),
        )
);
?>
<br>
<div>
    <b>
        Jumlah Siswa Sudah On-Desk : 
        <?php
        echo ModSiswa::model()->countByAttributes(array(
            'status_ondesk' => true,
        ));
        ?>
    </b>
</div>
