<?php
echo CHtml::dropDownList('ModSiswa[status_observasi]', $model->status_observasi, array(
    '0' => 'Belum Observasi',
    '1' => 'Sudah Observasi',
    'empty' => 'VIEW ALL',
    '' => '-------------',
), array(
    'class' => 'form-control',
));
