<?php

$this->pageTitle = 'Data Observasi';

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'no_surat_tugas',
        'tempat_observasi',
        array(
            'name' => 'tgl_penyerahan_berkas',
            'filter' => '',
        ),
        array(
            'name' => 'tgl_observasi',
            'filter' => '',
        ),
        array(
            'name' => 'tgl_pengumpulan',
            'filter' => '',
        ),
        array(
            'header' => 'Petugas',
            'value' => '$data->idUser->username',
        ),
        'keterangan',
        array(
            'header' => 'List Siswa',
            'type' => 'raw',
            'value' => array($this, 'getListSiswaButton'),
        ),
    ),
        )
);
