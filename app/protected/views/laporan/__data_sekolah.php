<br>
<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Sekolah Siswa',
    'headerIcon' => 'home',
));

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'npsn',
        'nama_sekolah',
        'alamat',
        'kota',
        'provinsi',
    )
));

$this->endWidget();