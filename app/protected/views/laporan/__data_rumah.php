<br>
<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Rumah Siswa',
    'headerIcon' => 'home',
));

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'kepemilikan',
        'luas_tanah',
        'luas_bangunan',
        'listrik',
        'sumber_air',
        'mck',
        'bahan_atap',
        'bahan_lantai',
        'bahan_tembok',
        'tahun_perolehan',
        'jarak_kekota',
    )
));

$this->endWidget();
