<?php
$this->pageTitle = 'Data Observasi Siswa';
$this->breadcrumbs = array(
    'List Observasi' => array('listObservasi'),
    'List Siswa'
);

$dropDown = CHtml::dropDownList('ModSiswa[status_observasi]', '', array(
    '0' => 'Belum Observasi',
    '1' => 'Sudah Observasi',
    'empty' => 'VIEW ALL',
    '' => '-------------',
), array(
    'class' => 'form-control',
));

echo CHtml::link('Kembali', $this->createUrl('listObservasi'), array(
    'class' => 'btn btn-default',
        )
);

echo '<br><br>';

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Siswa',
    'headerIcon' => 'check',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'ajaxUpdate' => true,
    'template' => '{items}',
    'columns' => array(
        'kap',
        'nisn',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'agama',
        array(
            'name' => 'status_observasi',
            'type' => 'raw',
            'value' => array($this, 'getStatusObservasi'),
            'filter' => $dropDown,
        ),
    ),
        )
);

$this->endWidget();

?>
<br>
<div>
    <b>
        Jumlah Siswa Sudah Observasi : 
        <?php
        echo ModSiswa::model()->countByAttributes(array(
            'status_observasi' => true,
            'id_observasi' => $model->id_observasi,
        ));
        ?>
    </b>
</div>