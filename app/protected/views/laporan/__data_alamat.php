<br>
<?php

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Alamat Siswa',
    'headerIcon' => 'home',
));

$this->widget(
        'booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'alamat',
        'kota',
        'provinsi',
    )
));

$this->endWidget();