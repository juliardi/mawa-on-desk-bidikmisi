<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#log-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'Manajemen Log';

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'log-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'User',
            'value' => '$data->idUser->username',
        ),
        'waktu',
        'aktivitas',
        'keterangan',
    ),
));