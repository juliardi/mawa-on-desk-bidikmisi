
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="Shortcut Icon" type="image/ico" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/favicon.png" />
        <title><?php echo Yii::app()->name; ?></title>

        <!-- Bootstrap Core CSS -->
        <!--<link href="/assets/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- MetisMenu CSS -->
        <!--<link href="/assets/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">-->

        <!-- Timeline CSS -->
        <!--<link href="/assets/css/plugins/timeline.css" rel="stylesheet">-->

        <!-- Custom CSS -->
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!--<link href="/assets/css/plugins/morris.css" rel="stylesheet">-->

        <!-- Custom Fonts -->
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo Yii::app()->homeUrl;  ?>"><?php echo Yii::app()->name ?></a>
                </div>
                <!-- /.navbar-header -->


                <!-- /.navbar-top-links -->