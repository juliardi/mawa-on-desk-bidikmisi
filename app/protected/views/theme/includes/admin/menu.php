<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">

        <?php
        $this->widget(
                'booster.widgets.TbMenu', array(
            'type' => 'list',
            'items' => $this->menu,
                )
        );
        ?>

    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

