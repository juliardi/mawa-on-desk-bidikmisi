<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//theme/includes/admin/header'); ?>
<?php $this->endContent(); ?>
<?php $this->beginContent('//theme/includes/admin/menu'); ?>
<?php $this->endContent(); ?>

<div id="content">

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $this->pageTitle; ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if (isset($this->breadcrumbs)): ?>
                                    <?php
                                    $this->widget('zii.widgets.CBreadcrumbs', array(
                                        'links' => $this->breadcrumbs,
                                    ));
                                    ?><!-- breadcrumbs -->
                                <?php endif ?>
                                    <br>
                                <?php echo $content; ?>

                            </div>

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
</div><!-- content -->

<?php $this->beginContent('//theme/includes/admin/footer'); ?>
<?php $this->endContent(); ?>

<!--<script src="/assets/js/plugins/morris/raphael.min.js"></script>
<script src="/assets/js/plugins/morris/morris.min.js"></script>
<script src="/assets/js/plugins/morris/morris-data.js"></script>-->
</body>

</html>