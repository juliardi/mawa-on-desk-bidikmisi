<?php
$this->pageTitle = 'Pilih Tahun Aktif';

?>

<form class="well col-lg-7 form-horizontal" method="POST" action="<?php echo Yii::app()->createUrl('tahunAktif/pilihTahun'); ?>">
    <?php $this->renderPartial('__label_notif', compact('notifikasi')) ?>
    <div class="form-group">
        <label class="col-sm-3 control-label required" for="ModTahunAktif_id">Pilih : </label>
        <div class="col-sm-9">
            <?php $this->renderPartial('__option_tahun'); ?>
        </div>
    </div>
    <button class="btn btn-primary btn-block btn btn-default" id="yw1" type="submit" name="yt0">Aktifkan Tahun</button>
</form>