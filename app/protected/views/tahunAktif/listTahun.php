<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#tahun-aktif-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'List Tahun';

$tahunAktif = ModTahunAktif::getTahunAktif();

$controller = $this;

echo CHtml::link('Tambah Tahun', array('tahunAktif/addTahun'), array(
    'class' => 'btn btn-primary',
));

echo '<br><br>';

if(empty($tahunAktif)) {
   echo '<span class="label label-danger" style="font-size: 15px;">Pilih Tahun Aktif!</span>' ;
}

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'tahun-aktif-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        'tahun',
        array(
            'header' => 'Status',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__status_tahun', compact('data'), true);
            },
        ),
        array(
            'header' => 'Delete',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__delete_button', compact('data'), true);
            },
        ),
    ),
));