<?php

$this->pageTitle = "Tambah Tahun";

echo CHtml::link('Kembali', Yii::app()->createUrl('tahunAktif/listTahun'), array(
    'class' => 'btn btn-default',
));

echo '<br><br>';

$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'horizontalForm',
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well col-lg-7'), // for inset effect
        )
);

echo $form->datePickerGroup($model, 'tahun', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'yyyy',
            'read-only' => 'true',
        )
    ),
));

echo $form->hiddenField($model, 'status', array(
    'value' => ModTahunAktif::STATUS_NON_AKTIF,
));

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Insert',
    'htmlOptions' => array(
        'class' => 'btn btn-primary btn-block',
    ),
));

$this->endWidget();