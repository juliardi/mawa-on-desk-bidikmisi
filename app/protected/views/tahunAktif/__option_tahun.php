<?php

$idTahunAktif = ModTahunAktif::getIdTahunAktif();
$listTahun = ModTahunAktif::getListTahun();

?>

<select id="ModTahunAktif_id" name="ModTahunAktif[id]" class="form-control">
<?php
foreach ($listTahun as $key => $tahun) {
    $option = '<option value="';
    $option .= $key;
    $option .= '" ';

    if ($key == $idTahunAktif) {
        $option .= 'selected="" ';
    }

    $option .= ">";

    $option .= $tahun;
    $option .= '</option>';
    echo $option;
}
?>
</select>