<?php $url = Yii::app()->createUrl('setting/deleteSetting');
    $script = "return confirm('Hapus Setting ini?')";
?>

<form method="POST" action="<?php echo $url; ?>" onsubmit="<?php echo $script; ?>">
    <input type="hidden" name="ModSetting[id]" value="<?php echo $data->id_setting; ?>">
    <input type="submit" name="delete" value="Delete" class="btn btn-danger">
</form>