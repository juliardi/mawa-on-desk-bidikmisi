<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#setting-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'List Setting';

echo CHtml::link('Tambah Setting', array('setting/addSetting'), array(
    'class' => 'btn btn-primary',
));

echo '<br>';

$controller = $this;

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'setting-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        'nama_setting',
        'nilai',
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
        array(
            'header' => 'Delete',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__delete_button', compact('data'), true);
            },
        ),
    ),
));