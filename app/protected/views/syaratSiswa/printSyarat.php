<?php
$this->pageTitle = "Print Persyaratan Siswa";

echo CHtml::link('Kembali', Yii::app()->createUrl('syaratSiswa/listSyaratSiswa', array('kap'=>$model->kap)), array(
    'class' => 'btn btn-default',
));

echo "&nbsp;";

echo CHtml::button('Print', array(
    'class' => 'btn btn-primary',
    'onclick' => 'printCetak()',
));
?>

<br><br>
<div id="printSyarat">
    <?php echo $this->renderPartial('__print_syarat', compact(array('model', 'syaratSiswa')), true); ?>
</div>

<script>
function printCetak() {
        var content = document.getElementById('printSyarat');
        var winPrint = window.open('', '', 'width=1600, height=700, scrollbars=1, menuBar=1');

        var str = '<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/bootstrap.min.css" type="text/css">';
        str += '<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" type="text/css">';
        str += '<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/home-style.css" type="text/css">';
        str += '<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/blog-style.css" type="text/css">';
        str += content.innerHTML;

        winPrint.document.write(str);
        winPrint.document.close();
        winPrint.focus();
        winPrint.window.print();
        var winPrint = window.close('', '', 'width=800, height=700, scrollbars=1, menuBar=1');

    }
</script>
