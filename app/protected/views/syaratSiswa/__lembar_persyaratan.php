<span>&nbsp;&nbsp;&nbsp;
</span>
<table
    style="text-align: left; width: 604px; height: 50px; font-size: 12px;"
    cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td style="vertical-align: top; width: 255px;">
                <span>LEMBAR PANTAUAN</span><br>
            </td>
            <td style="vertical-align: top;"><br>
            </td>
            <td style="vertical-align: top;">
                <table
                    style="text-align: left; width: 253px; height: 40px; 
                    margin-left: auto; margin-right: 0px; font-size: 12px;"
                    border="1" cellspacing="2">
                    <tbody>
                        <tr>
                            <td style="text-align: center;">
                                No Berkas<br>
                                <?php echo $model->no_berkas; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
            </td>
        </tr>
    </tbody>
</table>

<span>PERSYARATAN ON DESK REGISTRASI BIDIKMISI 2015</span><br
    style="font-family: Arial;">
<span style="font-weight: bold;">
    Pendaftaran Daring
</span>
<span style="font-family: Arial;"> </span>
<span style="font-weight: bold;">
    (ON-LINE) - SNMPTN / SBMPTN / MANDIRI
    <br>
</span><br style="font-family: Arial;">
<table
    style="text-align: left; width: 650px; height: 120px; font-size: 12px"
    border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr style=" height: 10px;">
            <td style="vertical-align: top; width: 289px;">
                NAMA PESERTA<br>
            </td>
            <td style="vertical-align: top; width: 23px;">
                :<br>
            </td>
            <td style="vertical-align: top; width: 310px;">
                <?php echo $model->nama; ?>
                <br>
            </td>
        </tr>
        <tr style=" height: 10px;">
            <td style="vertical-align: top; width: 289px;">
                NO PESERTA<br>
            </td>
            <td style="vertical-align: top; width: 23px;">:<br>
            </td>
            <td style="vertical-align: top; width: 310px;">
                <?php echo $model->no_pendaftaran; ?>
                <br>
            </td>
        </tr>
        <tr style=" height: 10px;">
            <td style="vertical-align: top; width: 289px;">
                NO PENDAFTARAN BIDIKMISI (KAP)<br>
            </td>
            <td style="vertical-align: top; width: 23px;">
                :<br>
            </td>
            <td style="vertical-align: top; width: 310px;">
                <?php echo $model->kap; ?>
                <br>
            </td>
        </tr>
        <tr style=" height: 20px;">
            <td style="vertical-align: top;">PROGRAM STUDI
                (DITERIMA)<br>
            </td>
            <td style="vertical-align: top;">:<br>
            </td>
            <td style="vertical-align: top;">
                <?php echo $model->idProdi->nama_prodi; ?>
                <br>
            </td>
        </tr>
    </tbody>
</table>

<table style="text-align: left; width: 100%; font-size: 12px;" border="1" cellpadding="2"
       cellspacing="2">
    <tbody>
        <tr>
            <td style="width: 50px; text-align: center;">NO<br>
            </td>
            <td style="width: 333px; text-align: center;">PERSYARATAN<br>
            </td>
            <td style="text-align: center; width: 62px;">BERKAS<br>
            </td>
            <td style="text-align: center;">KETERANGAN<br>
            </td>
        </tr>
        <?php
        $nomor = 1;
        foreach ($syaratSiswa as $modelSyaratSiswa) {
            ?>
            <tr>
                <td style="vertical-align: middle; text-align: center;">
                    <?php echo $nomor++; ?>
                    <br>
                </td>
                <td style="vertical-align: top; padding: 3px;">
                    <?php
                    echo $modelSyaratSiswa->idPersyaratan->syarat;
                    ?>
                    <br>
                </td>
                <td style="vertical-align: middle; text-align: center;">
                    <?php if ($modelSyaratSiswa->status == 1) { ?>
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <?php } ?>
                    <br>
                </td>
                <td style="vertical-align: top;  padding: 3px;">
                    <?php echo $modelSyaratSiswa->keterangan; ?>
                    <br>
                </td>
            </tr>
        <?php } ?> <!-- END FOREACH -->

    </tbody>
</table>
<br>
<div style="text-align: right;">Surakarta, <?php echo date('d F Y') ?><br>
    <br>
    <br>
    Panitia&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
    <br>
</div>
<br>