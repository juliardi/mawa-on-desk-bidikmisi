<?php
$this->pageTitle = 'List Syarat';

$controller = $this;

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Persyaratan Siswa KAP ' . $kap,
    'headerIcon' => 'check',
));

echo 'Centang jika berkas ada. Klik pada bagian Empty untuk memasukkan keterangan. '
 . 'Data tersimpan secara otomatis.<br><br>';

echo CHtml::link('Kembali', Yii::app()->createUrl('siswa/listSiswa'), array(
    'class' => 'btn btn-default',
));

echo "&nbsp;";

echo CHtml::link('Print', Yii::app()->createUrl('syaratSiswa/printSyarat', array(
            'kap' => $kap,
        )), array(
    'class' => 'btn btn-primary',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'template' => '{items}',
    'columns' => array(
        'syarat',
        array(
            'header' => 'Berkas ada?',
            'type' => 'raw',
            'value' => function($data) use ($controller, $kap) {
                    return $controller->renderPartial('__status_editable', compact(array('data', 'kap')), true);
            }
        ),
        array(
            'name' => 'keterangan',
            'type' => 'raw',
            'sortable' => false,
            'value' => function($data) use ($controller, $kap) {
                    return $controller->renderPartial('__keterangan_editable', compact(array('data', 'kap')), true);
            },
        ),
    ),
        )
);

$this->endWidget();

$url = Yii::app()->createUrl('syaratSiswa/updateSyarat');
?>

<script>
    $("input[type=checkbox]").click(function(e) {
        var cb = document.getElementById($(this).attr('id'));

        $.ajax({
            type: "POST",
            url: "<?php echo $url; ?>",
            data: {
                pk: $(this).attr('id'),
                value: (cb.checked ? 1 : 0),
                name: "status",
                scenario: "update"}
        });
    });
</script>