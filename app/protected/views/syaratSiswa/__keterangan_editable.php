<?php

$model = ModPersyaratanSiswa::getModelByKapPersyaratan($kap, $data->id_persyaratan);
$url = Yii::app()->createUrl('syaratSiswa/updateSyarat');

$edit = $this->widget('booster.widgets.TbEditableField', array(
    'type' => 'textarea',
    'model' => $model,
    'mode' => 'inline',
    'attribute' => 'keterangan',
    'value' => $model->keterangan,
    'url' => $url, //url for submit data
    'placement' => 'right',
    'emptytext' => 'Empty',
    'success' => 'js: function(response, newValue) {
        if (!response.success) 
            return response.msg;
        }',
    ));
