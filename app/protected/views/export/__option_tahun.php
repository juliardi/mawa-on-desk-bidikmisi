<?php

$idTahunAktif = ModTahunAktif::getIdTahunAktif();
$listTahun = ModTahunAktif::getListTahun();

?>

<select id="ModTahunAktif_id" name="ModTahunAktif[id_tahun_aktif]" class="form-control">
<?php
foreach ($listTahun as $key => $tahun) {
    $option = '<option value="';
    $option .= $key;
    $option .= '" >';
    $option .= $tahun;
    $option .= '</option>';
    echo $option;
}
?>
</select>