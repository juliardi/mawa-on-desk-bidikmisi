<?php
$this->pageTitle = 'Export Data Siswa';

?>

<form class="well col-lg-7 form-horizontal" method="POST" action="<?php echo Yii::app()->createUrl('export/exportSiswa'); ?>">
    <div class="form-group">
        <label class="col-sm-3 control-label required" for="ModTahunAktif_id">Pilih Tahun Aktif : </label>
        <div class="col-sm-9">
            <?php $this->renderPartial('__option_tahun'); ?>
        </div>
    </div>
    <button class="btn btn-primary btn-block btn btn-default" id="yw1" type="submit" name="yt0">Export Data Siswa</button>
</form>