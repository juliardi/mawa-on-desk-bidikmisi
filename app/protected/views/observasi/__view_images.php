<?php

if (!empty($data->scan_surat_tugas)) {
    $button = CHtml::button('Lihat Surat Tugas', array(
                'class' => 'btn btn-primary',
    ));

    $this->widget('ext.lyiightbox.LyiightBox2', array(
        'thumbnail' => $button,
        'image' => Yii::app()->getBaseUrl(true) . '/images/' . $data->scan_surat_tugas,
    ));
}
