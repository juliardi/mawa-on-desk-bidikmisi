<?php
$this->pageTitle = 'Tambah Siswa';
$this->breadcrumbs = array(
    'List Observasi' => array('listObservasi'),
    'List Siswa' => array('listSiswa', 'observasi' => $modelObservasi->id_observasi),
    'Tambah Siswa'
);

echo CHtml::link('Kembali', 
        $this->createUrl('listSiswa', array('observasi' => $modelObservasi->id_observasi)), 
        array(
            'class' => 'btn btn-default',
        )
);

echo '<br><br>';

$controller = $this;

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Siswa',
    'headerIcon' => 'check',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $modelSiswa->customSearch($modelObservasi->id_observasi),
    'type' => 'striped bordered condensed',
    'filter' => $modelSiswa,
    'ajaxUpdate' => true,
    'template' => '{items}',
    'columns' => array(
        'kap',
        'nisn',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'agama',
        array(
            'header' => 'Tambah Siswa',
            'type' => 'raw',
            'sortable' => false,
            'value' => function($data) use ($controller) {
        return $controller->renderPartial('__centang_siswa', array(
                    'data' => $data,
                        ), true);
    },
        ),
    ),
        )
);

$this->endWidget();

$url = Yii::app()->createUrl('observasi/addSiswa');
?>

<script>
    $("input[type=checkbox]").click(function(e) {
        var cb = document.getElementById($(this).attr('id'));

        $.ajax({
            type: "POST",
            url: "<?php echo $url; ?>",
            data: {
                Data: {
                    kap: $(this).attr('id'),
                    status: (cb.checked ? 1 : 0),
                    observasi: <?php echo $modelObservasi->id_observasi; ?>
                }
            }
        });
    });
</script>