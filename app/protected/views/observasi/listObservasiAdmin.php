<?php

$this->pageTitle = 'Data Observasi';
$dropDown = CHtml::dropDownList('ModObservasiSiswa[id_user]', '', $this->getPetugasList(true), array(
            'class' => 'form-control',
        ));

if (Yii::app()->user->hasRole('admin')) {
    echo CHtml::link('Tambah Observasi', array('observasi/addObservasi'), array(
        'class' => 'btn btn-primary',
    ));

    echo '<br>';
}

$controller = $this;

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'no_surat_tugas',
        'tempat_observasi',
        array(
            'name' => 'tgl_penyerahan_berkas',
            'filter' => '',
        ),
        array(
            'name' => 'tgl_observasi',
            'filter' => '',
        ),
        array(
            'name' => 'tgl_pengumpulan',
            'filter' => '',
        ),
        array(
            'name' => 'id_user',
            'header' => 'Petugas',
            'value' => '$data->idUser->username',
            'filter' => $dropDown,
        ),
        array(
            'name' => 'id_tahun_aktif',
            'header' => 'Tahun Aktif',
            'filter' => '',
            'value' => '$data->idTahunAktif->tahun',
        ),
        'keterangan',
        array(
            'header' => 'Scan Surat Tugas',
            'type' => 'raw',
            'sortable' => false,
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__view_images', compact('data'), true);
            },
        ),
        array(
            'header' => 'List Siswa',
            'type' => 'raw',
            'value' => array($this, 'getListSiswaButton'),
        ),
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
    ),
        )
);
