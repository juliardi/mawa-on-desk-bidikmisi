<?php

$this->pageTitle = "Tambah Observasi";

$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'horizontalForm',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
        'enctype' => 'multipart/form-data',
    ), // for inset effect
        ));

echo $form->errorSummary($model);
echo $form->textFieldGroup($model, 'no_surat_tugas');
echo $form->textFieldGroup($model, 'tempat_observasi');
echo $form->datePickerGroup($model, 'tgl_penyerahan_berkas', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'mm/dd/yyyy',
        )
    ),
));

echo $form->dropDownListGroup($model, 'id_user', array(
    'widgetOptions' => array(
        'data' => $this->getPetugasList(),
    )
));

echo $form->textAreaGroup($model, 'keterangan');

echo $form->fileFieldGroup($model, 'scan_surat_tugas');

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Insert',
    'htmlOptions' => array(
        'class' => 'btn btn-primary btn-block',
    ),
));

$this->endWidget();
