<?php
$this->pageTitle = 'Data Observasi Siswa';
$this->breadcrumbs = array(
    'List Observasi' => array('listObservasi'),
    'List Siswa'
);

echo CHtml::link('Kembali', $this->createUrl('listObservasi'), array(
    'class' => 'btn btn-default',
        )
);
?>
&nbsp;&nbsp;
<?php
if (Yii::app()->user->hasRole('admin')) {
    echo $this->getAddSiswaButton($modelObservasi);
}

echo '<br><br>';
$controller = $this;

$this->beginWidget('booster.widgets.TbPanel', array(
    'title' => 'Data Siswa',
    'headerIcon' => 'check',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $modelSiswa->search(),
    'type' => 'striped bordered condensed',
    'filter' => $modelSiswa,
    'ajaxUpdate' => true,
    'template' => '{items}',
    'columns' => array(
        'kap',
        'nisn',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'agama',
        array(
            'header' => 'Sudah Observasi?',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__centang_observasi', compact('data'), true);
            },
        ),
    ),
));

$this->endWidget();

$url = Yii::app()->createUrl('observasi/updateStatusObservasi');
?>

<script>
    $("input[type=checkbox]").click(function(e) {
        var cb = document.getElementById($(this).attr('id'));

        $.ajax({
            type: "POST",
            url: "<?php echo $url; ?>",
            data: {
                Data: {
                    kap: $(this).attr('id'),
                    status: (cb.checked ? 1 : 0),
                    observasi: <?php echo $modelObservasi->id_observasi; ?>
                }
            }
        });
    });
</script>