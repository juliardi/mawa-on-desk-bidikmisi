<?php

$this->pageTitle = "Update Observasi";

echo CHtml::link('Kembali', Yii::app()->createUrl('observasi/listObservasi'), array(
    'class' => 'btn btn-default',
));

echo '<br><br>';

$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'horizontalForm',
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well col-lg-7','enctype' => 'multipart/form-data',), // for inset effect
        )
);

echo $form->errorSummary($model);
echo $form->textFieldGroup($model, 'no_surat_tugas', array(
    'widgetOptions' => array(
        'read-only' => 'true',
    ),
));
echo $form->textFieldGroup($model, 'tempat_observasi', array(
    'widgetOptions' => array(
        'read-only' => 'true',
    ),
));
echo $form->datePickerGroup($model, 'tgl_penyerahan_berkas', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'dd-mm-yyyy',
            'read-only' => 'true',
        )
    ),
));
echo $form->datePickerGroup($model, 'tgl_observasi', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'dd-mm-yyyy',
        )
    ),
));
echo $form->datePickerGroup($model, 'tgl_pengumpulan', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'dd-mm-yyyy',
        )
    ),
));
echo $form->textAreaGroup($model, 'keterangan');

echo $form->fileFieldGroup($model, 'scan_surat_tugas');

if(!empty($model->scan_surat_tugas)) {
    echo CHtml::image(Yii::app()->getBaseUrl(true) . '/images/' . $model->scan_surat_tugas);
    echo '<br><br>';
}

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Update',
    'htmlOptions' => array(
        'class' => 'btn btn-primary btn-block',
    ),
));

$this->endWidget();
