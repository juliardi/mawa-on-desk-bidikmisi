<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $('#prodi-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->pageTitle = 'List Prodi';

echo CHtml::link('Tambah Prodi', array('prodi/addProdi'), array(
    'class' => 'btn btn-primary',
));

echo '<br>';

$controller = $this;

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'prodi-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered condensed',
    'filter' => $model,
    'columns' => array(
        'id_prodi',
        'kode_prodi',
        'nama_prodi',
        array(
            'name' => 'id_jenjang',
            'header' => 'Jenjang',
            'value' => '$data->idJenjang->nama_jenjang'
        ),
        array(
            'header' => 'Edit',
            'type' => 'raw',
            'value' => array($this, 'getEditButton'),
        ),
        array(
            'header' => 'Delete',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('__delete_button', compact('data'), true);
            },
        ),
    ),
));