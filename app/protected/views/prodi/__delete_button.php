<?php $url = Yii::app()->createUrl('prodi/deleteProdi');
    $script = "return confirm('Hapus Prodi ini?')";
?>

<form method="POST" action="<?php echo $url; ?>" onsubmit="<?php echo $script; ?>">
    <input type="hidden" name="id" value="<?php echo $data->id_prodi; ?>">
    <input type="submit" name="delete" value="Delete" class="btn btn-danger">
</form>