<?php

$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'verticalForm',
    'type' => 'vertical',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
    )
        ));

echo $form->errorSummary($model);
echo $form->textFieldGroup($model, 'kode_prodi');
echo $form->textFieldGroup($model, 'nama_prodi');
echo $form->dropDownListGroup($model, 'id_jenjang', array(
    'widgetOptions' => array(
        'data' => ModJenjang::getJenjangList(),
    ),
));

$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => ucfirst($model->scenario),
    'htmlOptions' => array(
        'class' => 'btn-primary btn-block',
    )
));

$this->endWidget();