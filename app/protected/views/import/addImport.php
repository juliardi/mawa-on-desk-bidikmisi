<?php

$this->pageTitle = 'Import Data Siswa';

$form = $this->beginWidget(
        'booster.widgets.TbActiveForm', array(
    'id' => 'horizontalForm',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'well col-lg-7',
        'enctype' => 'multipart/form-data'
    ), // for inset effect
        )
);

echo $form->errorSummary($model);
echo $form->fileFieldGroup($model, 'uploaded_file');
echo $form->dropDownListGroup($model, 'jalur_pendaftaran', array(
    'widgetOptions' => array(
        'data' => ModJalurPendaftaran::getListJalurPendaftaran(),
    )
));

$this->widget(
        'booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'label' => 'Upload',
    'htmlOptions' => array('class' => 'btn-primary btn-block'),
        )
);

echo '<br><br>';

echo CHtml::link('Download File Template', Yii::app()->getBaseUrl(true) . '/uploads/TEMPLATE.xlsx');

$this->endWidget();
