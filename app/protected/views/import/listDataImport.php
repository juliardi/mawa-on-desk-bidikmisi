<?php
$this->pageTitle = 'List Data Import';

$dropDown = CHtml::dropDownList('ModDataImport[status]', '', array(
    '0' => 'Belum Import',
    '1' => 'Sudah Import',
    '2' => 'Gagal Import',
    'empty' => 'VIEW ALL',
    '' => '-------------',
), array(
    'class' => 'form-control',
));

$url = Yii::app()->createUrl('import/importAll', array(
    'import' => $model->id_import,
));

echo CHtml::link('Kembali', array('import/listImport'), array(
    'class' => 'btn btn-default',
));
echo '&nbsp;&nbsp;';
echo CHtml::link('Import Semua Data', $url, array(
    'class' => 'btn btn-primary',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'kap',
        array(
            'header' => 'Prodi Diterima',
            'value' => '$data->idProdi->nama_prodi',
        ),
        array(
            'header' => 'Jenjang',
            'value' => '$data->idProdi->idJenjang->nama_jenjang',
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => array($this, 'getStatusImport'),
            'filter' => $dropDown,
        ),
        array(
            'header' => 'Import Data',
            'type' => 'raw',
            'value' => array($this, 'getImportButton'),
        ),
    ),
        )
);