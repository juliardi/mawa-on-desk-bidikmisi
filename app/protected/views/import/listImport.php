<?php
$this->pageTitle = 'List Import';

echo CHtml::link('Upload Data', array('import/addImport'), array(
    'class' => 'btn btn-primary',
));

$this->widget(
        'booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => 'striped bordered condensed',
    'ajaxUpdate' => true,
    'columns' => array(
        'nama_file',
        'tgl_import',
        'jumlah_data',
        array(
            'header' => 'Jalur Pendaftaran',
            'value' => '$data->idJalurPendaftaran->nama_jalur',
        ),
        array(
            'header' => 'Operator',
            'value' => '$data->idUser->username',
        ),
        array(
            'header' => 'List Data',
            'type' => 'raw',
            'value' => array($this, 'getListImportButton'),
        ),
    ),
        )
);