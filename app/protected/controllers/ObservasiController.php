<?php

/**
 * Description of ObservasiController
 *
 * @author juliardi
 */
class ObservasiController extends Controller {

    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'ObservasiController');
        
        return $accessRules;
        
    }

    public function actionAddObservasi() {

        $model = new ModObservasiSiswa();

        if (isset($_POST['ModObservasiSiswa'])) {
            $model->attributes = $_POST['ModObservasiSiswa'];
            $model->scan_surat_tugas = CUploadedFile::getInstance($model, 'scan_surat_tugas');
            $model->id_tahun_aktif = ModTahunAktif::getIdTahunAktif();
            
            if ($model->save()) {
                $this->redirect($this->createUrl('listObservasi'));
            }
        }

        $this->render('addObservasi', array(
            'model' => $model,
        ));
    }

    public function actionEditObservasi($observasi) {

        $model = $this->loadModel($observasi);

        if (isset($_POST['ModObservasiSiswa'])) {
            $model->attributes = $_POST['ModObservasiSiswa'];
            $model->scan_surat_tugas = CUploadedFile::getInstance($model, 'scan_surat_tugas');
            
            if ($model->save()) {
                $this->redirect($this->createUrl('listObservasi'));
            }
        }

        $this->render('editObservasi', array(
            'model' => $model,
        ));
    }

    public function actionListObservasi() {
        $model = new ModObservasiSiswa();

        $model->unsetAttributes();
        if (isset($_GET['ModObservasiSiswa'])) {
            $model->attributes = $_GET['ModObservasiSiswa'];
            if ($_GET['ModObservasiSiswa']['id_user'] == 'empty') {
                $model->id_user = '';
            }
        }

        if ($this->userIsPetugas()) {
            $model->id_user = Yii::app()->user->id;
            $model->id_tahun_aktif = ModTahunAktif::getIdTahunAktif();
            $this->render('listObservasiPetugas', compact('model'));
        } else {
            $this->render('listObservasiAdmin', compact('model'));
        }
    }

    public function actionAddSiswa() {

        if (isset($_POST['Data'])) {
            $data = $_POST['Data'];
            $model = ModSiswa::model()->findByPk($data['kap']);

            if ($data['status'] == 1) {
                $model->id_observasi = $data['observasi'];
            } else {
                $model->id_observasi = '';
            }

            if ($model->save()) {
                $this->sendSuccessJsonResponse();
            } else {
                $this->sendErrorJsonResponse();
            }
            return;
        }
        
        $this->redirect(array('observasi/listAddSiswa'));
    }

    public function actionListAddSiswa($observasi) {

        $modelObservasi = $this->loadModel($observasi);

        $modelSiswa = new ModSiswa('search');
        $modelSiswa->unsetAttributes();

        if (isset($_GET['Siswa'])) {
            $modelSiswa->attributes = $_GET['Siswa'];
        }
        $modelSiswa->id_tahun_aktif = ModTahunAktif::getIdTahunAktif();

        $this->render('listAddSiswa', array(
            'modelSiswa' => $modelSiswa,
            'modelObservasi' => $modelObservasi,
        ));
    }

    public function actionListSiswa($observasi) {
        $modelObservasi = $this->loadModel($observasi);

        if ($this->userIsPetugas() &&
                $modelObservasi->id_user != Yii::app()->user->id) {
            throw new CHttpException("Unauthorized access", 401);
        }

        $modelSiswa = new ModSiswa('search');
        $modelSiswa->unsetAttributes();
        $modelSiswa->id_observasi = $modelObservasi->id_observasi;

        if (isset($_GET['Siswa'])) {
            $modelSiswa->attributes = $_GET['Siswa'];
        }

        $this->render('listSiswa', array(
            'modelSiswa' => $modelSiswa,
            'modelObservasi' => $modelObservasi,
        ));
    }
    
    public function actionUpdateStatusObservasi() {
        
        if(Yii::app()->request->isAjaxRequest && isset($_POST['Data'])) {
            $data = $_POST['Data'];
            
            if($this->updateStatusObservasiSiswa($data['kap'], $data['status'])) {
                $this->updateTanggalPengumpulanBerkas($data['observasi']);
                
                $this->sendSuccessJsonResponse();
            } else {
                $this->sendErrorJsonResponse();
            }
            return;
        }
        
        $this->redirect(array('observasi/listSiswa'));
    }
    
    private function updateStatusObservasiSiswa($kap, $status) {
        
        $model = $this->loadSiswa($kap);
        
        $model->status_observasi = $status;
        
        return $model->save();
        
    }
    
    private function updateTanggalPengumpulanBerkas($id_observasi) {
        
        $jumlahBelumObservasi = ModSiswa::hitungSiswaBelumObservasi($id_observasi);
        
        $model = $this->loadModel($id_observasi);
        
        if($jumlahBelumObservasi == 0) {
            
            $model->tgl_pengumpulan = date('d-m-Y');
            
            $model->save();
        } else {
            
            $model->tgl_pengumpulan = '';
            
            $model->save();
        }
        
    }

    private function loadModel($id) {
        $model = ModObservasiSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, "Page not found");
        }

        return $model;
    }
    
    private function loadSiswa($id) {
        $model = ModSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, "Page not found");
        }

        return $model;
    }

    public function getAddSiswaButton($data) {
        $url = Yii::app()->createUrl('observasi/listAddSiswa', array(
            'observasi' => $data->id_observasi
        ));
        $button = CHtml::link('Tambah Siswa', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getListSiswaButton($data) {
        $url = Yii::app()->createUrl('observasi/listSiswa', array(
            'observasi' => $data->id_observasi
        ));
        $button = CHtml::link('List Siswa', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getEditButton($data) {
        $url = Yii::app()->createUrl('observasi/editObservasi', array(
            'observasi' => $data->id_observasi,
        ));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-default',
        ));

        return $button;
    }

    public function getPetugasList($emptySelection = false) {
        $model = ModUser::model()->findAllByAttributes(array(
            'id_role'=> ModRole::getIdRoleByName('petugas'),
        ));

        $list = CHtml::listData($model, 'id_user', 'username');

        if ($emptySelection) {
            $list['empty'] = 'VIEW ALL';
            $list[''] = '';
        }

        return $list;
    }

    private function userIsPetugas() {
        return (Yii::app()->user->hasRole("petugas"));
    }

}
