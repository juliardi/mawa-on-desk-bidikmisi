<?php

/**
 * Description of SyaratController
 *
 * @author juliardi
 */
class SyaratController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'SyaratController');
        
        return $accessRules;
        
    }
    
    public function actionAddSyarat() {
        
        $model = new ModPersyaratan();
        
        if(isset($_POST['ModPersyaratan'])) {
            $model->attributes = $_POST['ModPersyaratan'];
            
            if($model->save()) {
                $this->writeLog('Menambah Syarat', $model->attributes);
                $this->redirect($this->createUrl('syarat/listSyarat'));
            }
        }
        
        $this->render('addSyarat', compact('model'));
    }
    
    public function actionEditSyarat($id) {
        
        $model = $this->loadModel($id);
        
        if(isset($_POST['ModPersyaratan'])) {
            $model->attributes = $_POST['ModPersyaratan'];
            
            if($model->save()) {
                $this->writeLog('Mengedit Syarat', $model->attributes);
                $this->redirect($this->createUrl('syarat/listSyarat'));
            }
        }
        
        $this->render('editSyarat', compact('model'));
        
    }
    
    public function actionListSyarat() {
        
        $model = new ModPersyaratan('search');
        $model->unsetAttributes();
        
        if(isset($_GET['ModPersyaratan'])) {
            $model->attributes = $_GET['ModPersyaratan'];
        }
        
        $this->writeLog('Melihat List Syarat');
        $this->render('listSyarat', compact('model'));
    }
    
    public function actionDeleteSyarat() {
        
        if (Yii::app()->request->isPostRequest && isset($_POST['id'])) {
            $model = $this->loadModel($_POST['id']);
            $attributes = $model->attributes;
            
            $model->delete();
            $this->writeLog('Menghapus Syarat', $attributes);
            
        }

        $this->redirect($this->createUrl('syarat/listSyarat'));
    }
    
    public function actionUpdateStatus() {
        
        if (Yii::app()->request->isPostRequest && isset($_POST['ModPersyaratan'])) {
            $attributes = $_POST['ModPersyaratan'];
            $model = $this->loadModel($attributes['id_persyaratan']);
            
            $model->status = $attributes['status'];
            $model->save();
            $this->writeLog('Mengganti status Persyaratan', $model->attributes);
        }
        
        $this->redirect($this->createUrl('syarat/listSyarat'));
    }
    
    private function loadModel($id) {
        $model = ModPersyaratan::model()->findByPk($id);
        
        if(is_null($model)) {
            throw new CHttpException(404, 'Data tidak ditemukan');
        }
        
        return $model;
    }
    
    public function getEditButton($data) {
        $url = Yii::app()->createUrl('syarat/editSyarat', array('id' => $data->id_persyaratan));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }
    
}
