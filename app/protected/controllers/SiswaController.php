<?php

/**
 * Description of SiswaController
 *
 * @author juliardi
 */
class SiswaController extends Controller {

    public function accessRules() {

        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'SiswaController');

        return $accessRules;
    }

    public function actionAddSiswa() {

        $model = new KapForm();

        if (isset($_POST['KapForm'])) {
            $model->attributes = $_POST['KapForm'];

            if (ModSiswa::isSiswaExist($model->kap)) {
                $this->updateStatusOndeskSiswa($model->kap, ModSiswa::SUDAH_ON_DESK);

                $this->redirect($this->createUrl('showSiswa', array('kap' => $model->kap)));
            } else if ($model->save()) {

                $this->redirect($this->createUrl('showSiswa', array('kap' => $model->kap)));
            }
        }

        $this->render('addSiswa', compact('model'));
    }

    public function actionEditSiswa($kap) {

        $model = $this->loadSiswa($kap);

        if (isset($_POST['ModSiswa'])) {
            $model->attributes = $_POST['ModSiswa'];
            if ($model->save()) {
                $this->redirect($this->createUrl('showSiswa', array('kap' => $model->kap)));
            }
        }

        $this->render('editSiswa', compact('model'));
    }

    public function actionShowSiswa($kap) {

        $model = $this->loadSiswa($kap);

        $this->render('showSiswa', compact('model'));
    }

    public function actionListSiswa() {

        $model = new ModSiswa('search');

        $model->unsetAttributes();
        if (isset($_GET['ModSiswa'])) {
            $model->attributes = $_GET['ModSiswa'];
            if ($_GET['ModSiswa']['status_ondesk'] == 'empty') {
                $model->status_ondesk = '';
            }
        }
        $model->id_tahun_aktif = ModTahunAktif::getIdTahunAktif();

        $this->render('listSiswa', compact('model'));
    }

    public function actionDeleteSiswa() {

        if (Yii::app()->request->isPostRequest && isset($_POST['ModSiswa'])) {
            $model = $this->loadSiswa($_POST['ModSiswa']['kap']);
            $deleteAlamat = $model->idAlamat->delete();
            $deleteRumah = $model->idRumah->delete();
            $deleteKeluarga = ModKeluarga::deleteAllByKap($model->kap);
            $deletePersyaratan = ModPersyaratanSiswa::deleteAllByKap($model->kap);

            if ($deleteAlamat && $deleteKeluarga &&
                    $deleteRumah && $deletePersyaratan) {
                $model->delete();
            }
        }

        $this->redirect(array('siswa/listSiswa'));
    }

    public function actionEditSekolah($id) {
        $model = $this->loadSekolah($id);

        if (isset($_POST['ModSekolah'])) {
            $model->attributes = $_POST['ModSekolah'];
            if ($model->save()) {
                $this->redirect(array('siswa/listSiswa'));
            }
        }

        $this->render('editSekolah', compact('model'));
    }

    public function actionEditAlamat($id) {
        $model = $this->loadAlamat($id);

        if (isset($_POST['ModAlamat'])) {
            $model->attributes = $_POST['ModAlamat'];
            if ($model->save()) {
                $this->redirect($this->createUrl('showSiswa', array('kap' => $model->kap)));
            }
        }

        $this->render('editAlamat', compact('model'));
    }

    private function loadSiswa($id) {
        $model = ModSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }

    private function loadSekolah($id) {
        $model = ModSekolah::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }

    private function loadAlamat($id) {
        $model = ModAlamat::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }

    private function updateStatusOndeskSiswa($kap, $status) {

        $model = ModSiswa::model()->findByPk($kap);

        if (!is_null($model)) {
            $model->status_ondesk = $status;
        }

        return $model->save();
    }

    public function getSyaratButton(ModSiswa $data) {
        $url = Yii::app()->createUrl('syaratSiswa/listSyaratSiswa', array('kap' => $data->kap));
        $button = CHtml::link('Persyaratan', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getDetailButton(ModSiswa $data) {
        $url = Yii::app()->createUrl('siswa/showSiswa', array('kap' => $data->kap));
        $button = CHtml::link('Detail', $url, array(
                    'class' => 'btn btn-default',
        ));

        return $button;
    }

    public function getStatusOndesk(ModSiswa $data) {
        if ($data->status_ondesk == ModSiswa::SUDAH_ON_DESK) {
            return '<span class="label label-success">Sudah On-Desk</span>';
        } else {
            return '<span class="label label-danger">Belum On-Desk</span>';
        }
    }

}
