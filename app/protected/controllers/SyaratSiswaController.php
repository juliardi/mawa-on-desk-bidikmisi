<?php

/**
 * Description of SyaratSiswaController
 *
 * @author juliardi
 */
class SyaratSiswaController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'SyaratSiswaController');
        
        return $accessRules;
        
    }

    public function actionListSyaratSiswa($kap) {

        $modelSiswa = $this->loadSiswa($kap);
        $this->checkAndGenerateSyaratSiswa($modelSiswa->kap);

        $model = new ModPersyaratan('search');
        $model->status = true;

        $this->render('listSyarat', compact(array(
            'model', 'kap',
        )));
    }

    public function actionUpdateSyarat() {

        if (isset($_POST['pk'])) {
            $persyaratanSiswa = $this->loadSyaratSiswa($_POST['pk']);
            $persyaratanSiswa->setAttribute($_POST['name'], $_POST['value']);

            if ($persyaratanSiswa->save()) {
                $this->updateStatusOndeskSiswaByPersyaratan($persyaratanSiswa->kap);
                $this->sendSuccessJsonResponse();
            } else {
                $this->sendErrorJsonResponse();
            }

            return;
        }

        $this->redirect(array('syaratSiswa/listSyaratSiswa'));
    }

    public function actionPrintSyarat($kap) {

        $model = $this->loadSiswa($kap);
        $syaratSiswa = $this->generateArraySyaratSiswa($model);

        $this->render('printSyarat', compact(array('model', 'syaratSiswa')));
    }

    private function loadSiswa($id) {
        $model = ModSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }

    private function loadSyaratSiswa($id) {
        $model = ModPersyaratanSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }

    private function updateStatusOndeskSiswaByPersyaratan($kap) {

        $countSyarat = ModPersyaratanSiswa::countSyaratSiswaStatusTrue($kap);

        if ($countSyarat > 0) {
            $this->updateStatusOndeskSiswa($kap, ModSiswa::SUDAH_ON_DESK);
        } else {
            $this->updateStatusOndeskSiswa($kap, ModSiswa::BELUM_ON_DESK);
        }
    }

    private function updateStatusOndeskSiswa($kap, $status) {

        $model = ModSiswa::model()->findByPk($kap);

        if (!is_null($model)) {
            $model->status_ondesk = $status;
        }

        return $model->save();
    }

    private function checkAndGenerateSyaratSiswa($kap) {

        $syaratSiswaCount = ModPersyaratanSiswa::countSyaratSiswa($kap);
        $persyaratanCount = (int) ModPersyaratan::countSyaratAktif();

        if (($syaratSiswaCount != $persyaratanCount) || ($syaratSiswaCount == 0)) {
            $this->generateSyaratSiswa($kap);
        }
    }

    private function generateSyaratSiswa($kap) {
        $listPersyaratan = ModPersyaratan::model()->findAllByAttributes(array(
            'status' => ModPersyaratan::STATUS_AKTIF,
        ));

        foreach ($listPersyaratan as $modelPersyaratan) {
            if (!ModPersyaratanSiswa::isSyaratSiswaExist($kap, $modelPersyaratan->id_persyaratan)) {
                $this->addSyaratSiswa($kap, $modelPersyaratan);
            }
        }
    }

    private function addSyaratSiswa($kap, $modelPersyaratan) {
        $modelSyaratSiswa = new ModPersyaratanSiswa();

        $modelSyaratSiswa->attributes = array(
            'id_persyaratan' => $modelPersyaratan->id_persyaratan,
            'kap' => $kap,
            'status' => ModPersyaratanSiswa::STATUS_TIDAK_ADA,
        );

        $modelSyaratSiswa->save();
    }
    
    private function generateArraySyaratSiswa(ModSiswa $modelSiswa) {   
        $arrResult = array();
        foreach ($modelSiswa->persyaratanSiswas as $syarat) {
            if($syarat->idPersyaratan->status == true) {
                $arrResult[$syarat->id_persyaratan] = $syarat;
            }
        }
        // Urutkan berdasar id_persyaratan
        ksort($arrResult, SORT_ASC);
       
        return $arrResult;
    }

}
