<?php

/**
 * Description of ExportController
 *
 * @author juliardi
 */
class ExportController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'ExportController');
        
        return $accessRules;
        
    }

    public function actionExportSiswa() {

        if (isset($_POST['ModTahunAktif'])) {
            
            $export = new ExportData('ModSiswa', array(
                'status_ondesk' => true,
                'id_tahun_aktif' => $_POST['ModTahunAktif']['id_tahun_aktif'],
            ));
            $export->setExportedAttributes(array(
                'no_pendaftaran', 'nisn', 'kap', 'no_berkas', 'nama', 'tempat_lahir',
                'tgl_lahir', 'jenis_kelamin', 'agama', 'no_hp', 'email', 'tgl_daftar',
            ));
            $export->setRelationAttributes(array(
                'idProdi' => 'nama_prodi',
                'idJalurPendaftaran' => 'nama_jalur',
            ));
            
            $filename = $export->exportData();
            $this->addExport($filename);
            
            $this->redirect(Yii::app()->getBaseUrl(true) . '/exports/' . $filename);
        }
        
        $this->render('exportSiswa');
    }
    
    public function addExport($nama_file) {
        $model = new ModExport();
        
        $model->attributes = array(
            'nama_file' => $nama_file,
            'tgl_export' => date('d-m-Y'),
            'id_user' => Yii::app()->user->id,
        );
        
        $model->save();
        
    }
    
}
