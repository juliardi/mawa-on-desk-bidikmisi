<?php

/**
 * Description of ImportController
 *
 * @author juliardi
 */
class ImportController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'ImportController');
        
        return $accessRules;
        
    }

    public function actionAddImport() {

        $model = new ImportDataForm();

        if (isset($_POST['ImportDataForm'])) {
            $model->attributes = $_POST['ImportDataForm'];
            $model->uploaded_file = CUploadedFile::getInstance($model, 'uploaded_file');
            if ($model->validate() && $model->save()) {
                $this->redirect(array('import/listImport'));
            }
        }

        $this->render('addImport', compact('model'));
    }

    public function actionListImport() {

        $model = new ModImport();

        $model->unsetAttributes();
        if (isset($_GET['ModImport'])) {
            $model->attributes = $_GET['ModImport'];
        }

        $this->render('listImport', compact('model'));
    }

    public function actionListDataImport($import) {

        $modelImport = $this->loadModelImport($import);
        $model = new ModDataImport('search');

        $model->unsetAttributes();
        if (isset($_GET['ModDataImport'])) {
            $model->attributes = $_GET['ModDataImport'];
            if ($_GET['ModDataImport']['status'] == 'empty') {
                $model->status = '';
            }
        }
        $model->id_import = $modelImport->id_import;

        $this->render('listDataImport', compact('model'));
    }

    public function actionImportAll($import) {

        $modelImport = $this->loadModelImport($import);
        $modelData = ModDataImport::model()->findAllByAttributes(array(
            'id_import' => $modelImport->id_import,
        ));
        
        foreach ($modelData as $model) {
            if(!ModSiswa::isSiswaExist($model->kap)) {
                $this->insertData($model);
            }
        }
        
        $this->redirect($this->createUrl('listDataImport', array('import' => $modelImport->id_import)));
    }
    
    public function actionInsertData($data) {
        
        $model = $this->loadModelDataImport($data);
        if(!ModSiswa::isSiswaExist($model->kap)) {
            $this->insertData($model);
        }

        $this->redirect($this->createUrl('listDataImport', array('import' => $model->id_import)));
    }
    
    private function insertData($model) {
        
        $kapForm = new KapForm();
        $kapForm->attributes = array(
            'kap' => $model->kap,
            'jalur_pendaftaran' => $model->idImport->id_jalur_pendaftaran,
            'id_prodi' => $model->id_prodi,
            'status_ondesk' => ModSiswa::BELUM_ON_DESK,
        );

        if ($kapForm->save()) {
            $model->status = ModDataImport::STATUS_SUDAH_IMPORT;
        } else {
            $model->status = ModDataImport::STATUS_GAGAL_IMPORT;
        }

        return $model->save();
    }

    private function loadModelImport($id) {

        $model = ModImport::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, "Data tidak ditemukan");
        }

        return $model;
    }

    private function loadModelDataImport($idData) {

        $model = ModDataImport::model()->findByPk($idData);

        if (is_null($model)) {
            throw new CHttpException(404, "Data tidak ditemukan");
        }

        return $model;
    }

    public function getListImportButton($data) {
        $url = Yii::app()->createUrl('import/listDataImport', array('import' => $data->id_import));
        $button = CHtml::link('List Data', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getImportButton($data) {

        if ($data->status == ModDataImport::STATUS_SUDAH_IMPORT) {
            return '';
        }

        $url = Yii::app()->createUrl('import/insertData', array('data' => $data->id_import_data));
        $button = CHtml::link('Import Data', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }
    public function getStatusImport($data) {
        switch($data->status) {
            case ModDataImport::STATUS_SUDAH_IMPORT : 
                return '<span class="label label-success">Berhasil Diimport</span>';
            case ModDataImport::STATUS_GAGAL_IMPORT :
                return '<span class="label label-danger">Gagal Diimport</span>';
            case ModDataImport::STATUS_BELUM_IMPORT :
                return '<span class="label label-warning">Belum Diimport</span>';    
        }
    }

}
