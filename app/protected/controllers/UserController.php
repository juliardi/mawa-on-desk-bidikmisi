<?php

/**
 * Description of UserController
 *
 * @author juliardi
 */
class UserController extends Controller {

    public function accessRules() {

        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'UserController');

        return $accessRules;
    }

    public function actionAddUser() {

        $model = new ModUser();

        if (isset($_POST['ModUser'])) {
            $model->attributes = $_POST['ModUser'];
            $model->status = ModUser::STATUS_AKTIF;

            if ($model->save()) {
                $this->redirect($this->createUrl('user/listUser'));
            }
        }

        $this->render('addUser', compact('model'));
    }

    public function actionEditUser($id) {

        $model = $this->loadModel($id);
        $model->password = '';

        if (isset($_POST['ModUser'])) {
            $model->attributes = $_POST['ModUser'];
            if (empty($model->password)) {
                $model->password = $model->oldPassword;
            }

            if ($model->save()) {
                $this->redirect($this->createUrl('user/listUser'));
            }
        }

        $this->render('editUser', compact('model'));
    }

    public function actionGantiStatus() {

        if (Yii::app()->request->isPostRequest && isset($_POST['id'])) {
            $model = $this->loadModel($_POST['id']);

            $model->status = ($model->status == ModUser::STATUS_AKTIF) ?
                    ModUser::STATUS_NON_AKTIF :
                    ModUser::STATUS_AKTIF;

            if ($model->save()) {
                $this->redirect($this->createUrl('user/listUser'));
            } else {
                throw new CHttpException("Gagal mengganti status user");
            }
        }

        $this->redirect($this->createUrl('user/listUser'));
    }

    public function actionListUser() {

        $model = new ModUser('search');

        $model->unsetAttributes();

        if (isset($_GET['ModUser'])) {
            $model->attributes = $_GET['ModUser'];
            if($model->id_role == 'empty') {
                $model->id_role = '';
            }
        }

        $this->render('listUser', compact('model'));
    }

    private function loadModel($id) {

        $model = ModUser::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, "Data tidak ditemukan");
        }
        return $model;
    }

    public function getEditButton($data) {
        $url = Yii::app()->createUrl('user/editUser', array('id' => $data->id_user));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getStatusButton($data) {

        $url = Yii::app()->createUrl('user/gantiStatus');
        $status = ($data->status == ModUser::STATUS_AKTIF) ?
                "Non-Aktifkan" :
                "Aktifkan";
        $class = ($data->status == ModUser::STATUS_AKTIF) ?
                "btn btn-danger" :
                "btn btn-primary";

        $form = '<form method=POST action="' . $url . '" >'
                . '<input type=hidden name=id value=' . $data->id_user . '>'
                . '<input type=submit name="status" value="' . $status . '" class="' . $class . '">'
                . '</form>';

        return $form;
    }

}
