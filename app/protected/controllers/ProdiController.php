<?php

/**
 * Description of ProdiController
 *
 * @author juliardi
 */
class ProdiController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'ProdiController');
        
        return $accessRules;
        
    }

    public function actionAddProdi() {

        $model = new ModProdi();

        if (isset($_POST['ModProdi'])) {
            $model->attributes = $_POST['ModProdi'];
            if ($model->save()) {
                $this->writeLog('Menambah Prodi', $model->attributes);
                $this->redirect(array('prodi/listProdi'));
            }
        }

        $this->render('addProdi', compact('model'));
    }

    public function actionEditProdi($id) {

        $model = $this->loadModel($id);

        if (isset($_POST['ModProdi'])) {
            $model->attributes = $_POST['ModProdi'];
            if ($model->save()) {
                $this->writeLog('Mengedit Prodi', $model->attributes);
                $this->redirect(array('prodi/listProdi'));
            }
        }

        $this->render('editProdi', compact('model'));
    }

    public function actionListProdi() {

        $model = new ModProdi('search');

        $model->unsetAttributes();
        if (isset($_GET['ModProdi'])) {
            $model->attributes = $_GET['ModProdi'];
        }
        
        $this->writeLog('Melihat List Prodi');

        $this->render('listProdi', compact('model'));
    }

    public function actionDeleteProdi() {

        if (Yii::app()->request->isPostRequest && isset($_POST['delete'])) {
            
            $model = $this->loadModel($_POST['id']);

            if ($model->isSiswaEmpty() && $model->isDataImportEmpty()) {
                $attributes = $model->attributes;
                if($model->delete()) {
                    $this->writeLog('Menghapus Prodi', $attributes);
                }
            } else {
                $this->render('alertDelete');
            }
            
            $this->redirect(array('prodi/listProdi'));
            
        }
    }

    private function loadModel($id) {
        $model = ModProdi::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException("Page not found", 404);
        }

        return $model;
    }
    
    public function getEditButton($data) {
        $url = Yii::app()->createUrl('prodi/editProdi', array('id' => $data->id_prodi));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

}
