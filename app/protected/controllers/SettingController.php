<?php

/**
 * Description of SettingController
 *
 * @author juliardi
 */
class SettingController extends Controller {
    
    public function accessRules() {
        
        $rules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'SettingController');
        
        return $rules;
        
    }
    
    public function actionAddSetting() {
        
        $model = new ModSetting();
        
        if(isset($_POST['ModSetting'])) {
            $model->attributes = $_POST['ModSetting'];
            if($model->save()) {
                $this->redirect(array('setting/listSetting'));
            }
        }
        
        $this->render('addSetting', compact('model'));
        
    }
    
    public function actionEditSetting($id) {
        
        $model = $this->loadModel($id);
        
        if(isset($_POST['ModSetting'])) {
            $model->attributes = $_POST['ModSetting'];
            if($model->save()) {
                $this->redirect(array('setting/listSetting'));
            }
        }
        
        $this->render('editSetting', compact('model'));
        
    }
    
    public function actionListSetting() {
        
        $model = new ModSetting('search');
        
        $model->unsetAttributes();
        if(isset($_GET['ModSetting'])) {
            $model->attributes = $_GET['ModSetting'];
        }
        
        $this->render('listSetting', compact('model'));
    }
    
    public function actionDeleteSetting() {
        
        if(Yii::app()->request->isPostRequest && isset($_POST['ModSetting[id]'])) {
            $model = $this->loadModel($_POST['ModSetting[id]']);
            
            $model->delete();
        }
        
        $this->redirect(array('setting/listSetting'));
        
    }
    
    private function loadModel($id) {
        
        $model = ModSetting::model()->findByPk($id);
        
        if(is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }
        
        return $model;
    }
    
    public function getEditButton($data) {
        $url = Yii::app()->createUrl('setting/editSetting', array('id' => $data->id_setting));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }
}
