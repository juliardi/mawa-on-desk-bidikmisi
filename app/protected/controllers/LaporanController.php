<?php

/**
 * Description of LaporanController
 *
 * @author juliardi
 */
class LaporanController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'LaporanController');
        
        return $accessRules;
        
    }

    public function actionListSiswa() {

        $model = new ModSiswa('search');

        $model->unsetAttributes();
        if (isset($_GET['ModSiswa'])) {
            $model->attributes = $_GET['ModSiswa'];
            if ($model->status_ondesk == 'empty') {
                $model->status_ondesk = '';
            }
            if ($model->status_observasi == 'empty') {
                $model->status_observasi = '';
            }
        }

        $this->render('listSiswa', compact('model'));
    }

    public function actionShowSiswa($kap) {

        $model = $this->loadSiswa($kap);

        $this->render('showSiswa', compact('model'));
    }

    public function actionListObservasi() {

        $model = new ModObservasiSiswa('search');

        $model->unsetAttributes();
        if (isset($_GET['ModObservasiSiswa'])) {
            $model->attributes = $_GET['ModObservasiSiswa'];
        }

        $this->render('listObservasi', compact('model'));
    }

    public function actionListObservasiSiswa($observasi) {

        $modelObservasi = $this->loadObservasi($observasi);
        $model = new ModSiswa('search');

        $model->unsetAttributes();
        $model->id_observasi = $modelObservasi->id_observasi;
        if (isset($_GET['ModSiswa'])) {
            $model->attributes = $_GET['ModSiswa'];
            if ($model->status_observasi == 'empty') {
                $model->status_observasi = '';
            }
        }

        $this->render('listObservasiSiswa', compact('model'));
    }
    
    public function actionRekapData() {
        $this->render('rekapData');
    }

    private function loadSiswa($id) {
        $model = ModSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }
    
    private function loadObservasi($id) {
        $model = ModObservasiSiswa::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }

        return $model;
    }
    
    public function getDetailSiswaButton($data) {
        $url = Yii::app()->createUrl('laporan/showSiswa', array('kap' => $data->kap));
        $button = CHtml::link('Detail', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

    public function getStatusOndesk($data) {

        if($data->status_ondesk == ModSiswa::SUDAH_ON_DESK) {
            return '<span class="label label-success">Sudah On-Desk</span>';
        } else {
            return '<span class="label label-danger">Belum On-Desk</span>';
        }
        
    }
    
    public function getStatusObservasi($data) {
        if($data->status_observasi == ModSiswa::STATUS_SUDAH_OBSERVASI) {
            return '<span class="label label-success">Sudah Diobservasi</span>';
        } else {
            return '<span class="label label-danger">Belum Diobservasi</span>';
        }
    }
    
    public function getListSiswaButton($data) {
        $url = Yii::app()->createUrl('laporan/listObservasiSiswa', array(
            'observasi' => $data->id_observasi
        ));
        $button = CHtml::link('List Siswa', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }

}
