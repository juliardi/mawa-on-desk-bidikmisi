<?php

/**
 * Description of LogController
 *
 * @author juliardi
 */
class LogController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'LogController');
        
        return $accessRules;
        
    }
    
    public function actionListLog() {
        
        $model = new ModLog('search');
        
        $model->unsetAttributes();
        if(isset($_GET['ModLog'])) {
            $model->attributes = $_GET['ModLog'];
        }
        
        $this->render('listLog', compact('model'));
        
    }
    
    public function actionClearLog() {
        
    }
    
}
