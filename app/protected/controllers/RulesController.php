<?php

/**
 * Description of AccessController
 *
 * @author juliardi
 */
class RulesController extends Controller {
    
    public function accessRules() {
        
        $rules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'RulesController');
        
        return $rules;
        
    }
    
    public function actionAddRules() {
        
        $model = new ModAccessRules();
        
        if(isset($_POST['ModAccessRules'])) {
            $model->attributes = $_POST['ModAccessRules'];
            if($model->save()) {
                $this->redirect(array('rules/listRules'));
            }
        }
        
        $this->render('addRules', compact('model'));
        
    }
    
    public function actionEditRules($id) {
        
        $model = $this->loadModel($id);
        
        if(isset($_POST['ModAccessRules'])) {
            $model->attributes = $_POST['ModAccessRules'];
            if($model->save()) {
                $this->redirect(array('rules/listRules'));
            }
        }
        
        $this->render('editRules', compact('model'));
        
    }
    
    public function actionListRules() {
        
        $model = new ModAccessRules('search');
        
        $model->unsetAttributes();
        if(isset($_GET['ModAccessRules'])) {
            $model->attributes = $_GET['ModAccessRules'];
        }
        
        $this->render('listRules', compact('model'));
        
    }
    
    public function actionDeleteRules() {
        
        if (Yii::app()->request->isPostRequest && isset($_POST['id'])) {
            $model = $this->loadModel($_POST['id']);
            
            $model->delete();
            
        }

        $this->redirect($this->createUrl('rules/listRules'));
    }
    
    private function loadModel($id) {
        $model = ModAccessRules::model()->findByPk($id);
        
        if(is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }
        
        return $model;
    }
    
    public function getEditButton($data) {
        $url = Yii::app()->createUrl('rules/editRules', array('id' => $data->id_access_rules));
        $button = CHtml::link('Edit', $url, array(
                    'class' => 'btn btn-primary',
        ));

        return $button;
    }
    
}
