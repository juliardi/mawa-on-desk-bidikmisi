<?php

/**
 * Description of TahunAktifController
 *
 * @author juliardi
 */
class TahunAktifController extends Controller {
    
    public function accessRules() {
        
        $accessRules = AccessRulesGenerator::getAccessRules(Yii::app()->user->name, 'TahunAktifController');
        
        return $accessRules;
        
    }

    public function actionAddTahun() {
        $model = new ModTahunAktif();

        if (isset($_POST['ModTahunAktif'])) {
            $model->attributes = $_POST['ModTahunAktif'];
            
            $tahunAktif = ModTahunAktif::getTahunAktif();
            if(empty($tahunAktif)) {
                $model->status = ModTahunAktif::STATUS_AKTIF;
            }
            
            if ($model->save()) {
                $this->redirect(array('tahunAktif/pilihTahun'));
            }
        }

        $this->render('addTahun', compact('model'));
    }
    
    public function actionPilihTahun() {
        
        $notifikasi = false;
        
        if(isset($_POST['ModTahunAktif'])) {
            $this->aktifkanTahunAktif($_POST['ModTahunAktif']['id']);
            $notifikasi = true;
        }

        $this->render('pilihTahun', compact('notifikasi'));
        
    }

    public function actionListTahun() {
        $model = new ModTahunAktif('search');

        $model->unsetAttributes();
        if (isset($_GET['ModTahunAktif'])) {
            $model->attributes = $_GET['ModTahunAktif'];
        }

        $this->render('listTahun', compact('model'));
    }
    
    public function actionDeleteTahun() {
        
        if(Yii::app()->request->isPostRequest && isset($_POST['ModTahunAktif'])) {
            $model = $this->loadModel($_POST['ModTahunAktif']['id']);
            if(!$model->isTahunUsed()) {
                $model->delete();
            }
        }
        
        $this->redirect(array('tahunAktif/listTahun'));
    }
    
    private function loadModel($id) {
        $model = ModTahunAktif::model()->findByPk($id);
        
        if(is_null($model)) {
            throw new CHttpException('Page not found', 404);
        }
        
        return $model;
    }

    private function aktifkanTahunAktif($id) {

        $models = ModTahunAktif::model()->findAll();

        foreach ($models as $model) {

            if ($model->id_tahun_aktif == $id) {
                $model->status = ModTahunAktif::STATUS_AKTIF;
            } else {
                $model->status = ModTahunAktif::STATUS_NON_AKTIF;
            }

            $model->save();
        }
    }

}
