<?php

/**
 * Description of KapForm
 *
 * @author juliardi
 */
class KapForm extends CFormModel {

    public $kap;
    public $no_pendaftaran;
    public $jalur_pendaftaran;
    public $id_prodi;
    public $status_ondesk;

    public function rules() {
        return array(
            array('kap, jalur_pendaftaran, id_prodi, status_ondesk', 'required'),
            array('kap', 'length', 'max' => 12),
            array('no_pendaftaran', 'length', 'max' => 20),
            array('kap, no_pendaftaran, jalur_pendaftaran, status_ondesk, id_prodi', 'numerical', 'integerOnly' => true),
        );
    }

    public function attributeLabels() {
        return array(
            'kap' => 'Nomor KAP',
            'jalur_pendaftaran' => 'Jalur Pendaftaran',
            'id_prodi' => 'Prodi Diterima',
            'no_pendaftaran' => 'Nomor Pendaftaran',
        );
    }

    public function retrieveData() {
        try {
            return $this->retrieveDataFromServer();
        } catch (Exception $ex) {
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR);
            $this->writeLog('Error Retrieve Data', $ex);
            $this->addError('kap', 'Gagal melakukan koneksi ke server bidikmisi');
            return null;
        }
    }

    private function retrieveDataFromServer() {
        $service = new KapService();
        $data = $service->cekKAP($this->kap);

        if ($data->status == 0) {
            return $data->result;
        } else {
            $this->addError('kap', $service->getErrorString($data->status));
            return null;
        }
    }

    public function save() {
        $data = $this->retrieveData();
        
        if(!empty($this->no_pendaftaran) && !is_null($data)) {
            $komparasi = strcmp($this->no_pendaftaran, trim($data->no_pendaftaran));
            if($komparasi != 0) {
                $this->addError('no_pendaftaran', 'Data Tidak Valid. Nomor Pendaftaran tidak cocok. strcmp = '. $komparasi . ' data->no_pendaftaran='.$data->no_pendaftaran);
                return false;
            }
        }

        if (!is_null($data)) {
            return $this->saveDataSekolah($data);
        }

        return false;
    }
    
    public function saveSiswa($data) {
        
        $modelSiswa = new ModSiswa();

        $modelSiswa->attributes = array(
            'no_pendaftaran' => trim($data->no_pendaftaran),
            'kap' => trim($data->kap),
            'nisn' => trim($data->nisn),
            'nama' => $data->nama,
            'tempat_lahir' => $data->tempat_lahir,
            'tgl_lahir' => $data->tgl_lahir,
            'tgl_daftar' => $data->tgl_daftar,
            'jenis_kelamin' => $data->jenis_kelamin,
            'agama' => $data->agama,
            'no_berkas' => $this->getNoBerkasSekarang(),
            'no_hp' => $data->no_hp,
            'email' => $data->email,
            'kode_sekolah' => $data->sekolah->npsn,
            'id_prodi' => $this->id_prodi,
            'id_jalur_pendaftaran' => $this->jalur_pendaftaran,
            'id_tahun_aktif' => ModTahunAktif::getIdTahunAktif(),
            'status_ondesk' => $this->status_ondesk,
            'status_observasi' => ModSiswa::STATUS_BELUM_OBSERVASI,
        );
        
        if($modelSiswa->save()) {
            return $this->saveOthers($data);
        } else {
            $this->addErrors($modelSiswa->errors);
        }
    }
    
    private function saveOthers($data) {
        $saveRumah = $this->saveDataRumah($data);
        $saveAlamat = $this->saveDataAlamat($data);
        $saveKeluarga = $this->saveKeluarga($data);
        
        return ($saveRumah && $saveAlamat && $saveKeluarga);
    }

    private function saveDataSekolah($data) {

        if (!ModSekolah::isSekolahExist($data->sekolah->npsn)) {
            $modelSekolah = new ModSekolah();

            $modelSekolah->attributes = array(
                'npsn' => $data->sekolah->npsn,
                'nama_sekolah' => $data->sekolah->nama_sekolah,
                'alamat' => $data->sekolah->alamat,
                'kota' => $data->sekolah->kota,
                'provinsi' => $data->sekolah->provinsi,                
            );

            if ($modelSekolah->save()) {
                return $this->saveSiswa($data);
            } else {
                $this->addErrors($modelSekolah->errors);
            }
        } else {
            return $this->saveSiswa($data);
        }
    }

    private function saveDataRumah($data) {
        $rumah = new ModRumah();

        $rumah->attributes = array(
            'kepemilikan' => $data->rumah->kepemilikan,
            'luas_tanah' => $data->rumah->luas_tanah,
            'luas_bangunan' => $data->rumah->luas_bangunan,
            'listrik' => $data->rumah->listrik,
            'sumber_air' => $data->rumah->sumber_air,
            'mck' => $data->rumah->mck,
            'bahan_atap' => $data->rumah->bahan_atap,
            'bahan_lantai' => $data->rumah->bahan_lantai,
            'bahan_tembok' => $data->rumah->bahan_tembok,
            'tahun_perolehan' => $data->rumah->tahun_perolehan,
            'jarak_kekota' => $data->rumah->jarak_kekota,
            'kap' => $data->kap,
        );

        if ($rumah->save()) {
            return true;
        } else {
            $this->addErrors($rumah->errors);
        }
    }

    private function saveDataAlamat($data) {
        $alamat = new ModAlamat();

        $alamat->attributes = array(
            'alamat' => $data->alamat,
            'kota' => $data->kota,
            'provinsi' => $data->provinsi,
            'kap' => $data->kap,
        );

        if ($alamat->save()) {
            return true;
        } else {
            $this->addErrors($alamat->errors);
        }
    }

    private function saveKeluarga($data) {
        $saveAyah = false;
        $saveIbu = false;
        
        $dataKeluarga = $data->keluarga;
        
        if(isset($dataKeluarga->ayah)) {
            $saveAyah = $this->saveDataAyah($dataKeluarga->ayah, $data->kap);
        } else {
            $saveAyah = true;
        }
        if(isset($dataKeluarga->ibu)) {
            $saveIbu = $this->saveDataIbu($dataKeluarga->ibu, $data->kap);
        } else {
            $saveIbu = true;
        }
        
        return $saveAyah && $saveIbu;
    }
    
    private function saveDataAyah($dataAyah, $kap) {
        return $this->saveDataKeluarga($dataAyah, $kap, ModKeluarga::STATUS_AYAH);
    }
    
    private function saveDataIbu($dataIbu, $kap) {
        return $this->saveDataKeluarga($dataIbu, $kap, ModKeluarga::STATUS_IBU);
    }

    private function saveDataKeluarga($dataKeluarga, $kap, $statusHubungan) {

        $keluarga = new ModKeluarga();

        $keluarga->attributes = array(
            'nama' => $dataKeluarga->nama,
            'pendidikan' => $dataKeluarga->pendidikan,
            'pekerjaan' => $dataKeluarga->pekerjaan,
            'penghasilan' => $dataKeluarga->penghasilan,
            'status' => $dataKeluarga->status,
            'status_hubungan' => $statusHubungan,
            'kap' => $kap,
        );

        if ($keluarga->save()) {
            return true;
        } else {
            $this->addErrors($keluarga->errors);
        }
    }
    
    private function getNoBerkasSekarang() {
        switch ($this->jalur_pendaftaran) {
            case 1 : {
                $noBerkas = (int) ModSetting::getSettingValue('NO_BERKAS_SN');
                ModSetting::setSettingValue('NO_BERKAS_SN', $noBerkas+1);
                break;
            }
            case 2 : {
                $noBerkas = (int) ModSetting::getSettingValue('NO_BERKAS_SB');
                ModSetting::setSettingValue('NO_BERKAS_SB', $noBerkas+1);
                break;
            }
            case 3 : {
                $noBerkas = (int) ModSetting::getSettingValue('NO_BERKAS_MANDIRI');
                ModSetting::setSettingValue('NO_BERKAS_MANDIRI', $noBerkas+1);
                break;
            }
        }
        
        return $noBerkas;
    }
    
    protected function writeLog($activity, $keterangan = null) {
        
        $id_user = Yii::app()->user->id;
        if(!is_null($keterangan)) {
            $keterangan = CVarDumper::dumpAsString($keterangan);
        }
        ModLog::writeLog($id_user, $activity, $keterangan);
        
    }

}
