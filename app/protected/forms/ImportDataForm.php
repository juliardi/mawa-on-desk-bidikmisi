<?php

/**
 * Description of ImportDataForm
 *
 * @author juliardi
 */
class ImportDataForm extends CFormModel {

    public $uploaded_file;
    public $jalur_pendaftaran;
    private $filename;
    private $filepath;

    public function rules() {
        return array(
            array('uploaded_file, jalur_pendaftaran', 'required'),
            array('uploaded_file', 'file', 'types' => 'xls, xlsx'),
            array('uploaded_file', 'upload'),
            array('jalur_pendaftaran', 'numerical', 'integerOnly' => true),
        );
    }

    public function attributeLabels() {
        return array(
            'uploaded_file' => 'File'
        );
    }

    /**
     * Method yang digunakan untuk validasi Yii
     * @return boolean 
     */
    public function upload() {
        try {
            $this->filename = $this->generateFilename();
            $this->filepath = Yii::app()->params['uploadDir'] . $this->filename;
            return $this->uploaded_file->saveAs($this->filepath);
        } catch (Exception $ex) {
            $this->writeLog('Error Upload Data', $ex);
        }
    }

    /**
     * Method untuk menggenerate nama file yang diupload
     * @return String Nama file yang disimpan di server
     */
    private function generateFilename() {
        $date = date('His-dmY');
        $userId = Yii::app()->user->id;
        $ext = $this->uploaded_file->getExtensionName();

        return 'import-' . $date . '-' . $userId . '.' . $ext;
    }

    public function save() {

        $idImport = $this->saveToModelImport();
        if (is_null($idImport)) {
            return false;
        }

        return $this->readAndSaveData($idImport);
    }

    /**
     * Method untuk membaca data dari file excel
     * @param type $idImport
     * @return type
     */
    private function readAndSaveData($idImport) {

        $excel = $this->loadPHPExcel();
        $sheet = $excel->getSheet();

        $valid = true;
        $i = 2;
        $jumlahData = 0;
        while ($valid) {
            $kap = $sheet->getCell('A' . $i)->getValue();
            $kode_prodi = $sheet->getCell('B' . $i++)->getValue();

            if (empty($kode_prodi) || empty($kap)) {
                $valid = false;
                break;
            }

            if (!$this->validateKodeProdi($kode_prodi)) {
                $valid = false;
                $this->addError('uploaded_file', 'Kode Prodi tidak valid');
                break;
            }

            $valid = $this->saveToDataImport(array(
                'kap' => $kap,
                'id_prodi' => $kode_prodi,
                'id_import' => $idImport,
                'status' => ModDataImport::STATUS_BELUM_IMPORT,
            ));

            $jumlahData++;
        }

        return (count($this->errors) == 0) && $this->updateJumlahDataImport($idImport, $jumlahData);
    }

    /**
     * Method untuk memvalidasi kode prodi
     * @param type $kode_prodi
     */
    private function validateKodeProdi($kode_prodi) {
        $model = ModProdi::model()->findByPk($kode_prodi);

        return !is_null($model);
    }

    /**
     * Method untuk menyimpan data ke tabel tbl_import
     * @return null
     */
    private function saveToModelImport() {

        $model = new ModImport();

        $model->attributes = array(
            'nama_file' => $this->filename,
            'tgl_import' => date('d-m-Y'),
            'id_user' => Yii::app()->user->id,
            'id_jalur_pendaftaran' => $this->jalur_pendaftaran,
        );

        if ($model->save()) {
            return $model->id_import;
        } else {
            $this->addErrors($model->errors);
            return null;
        }
    }

    /**
     * Method untuk menyimpan data ke tabel tbl_data_import
     * @param type $data
     * @return boolean
     */
    private function saveToDataImport($data) {

        $model = new ModDataImport();
        $model->attributes = $data;

        if ($model->save()) {
            return true;
        } else {
            $this->addErrors($model->errors);
            return false;
        }
    }

    /**
     * Method untuk mengupdate kolom jumlah_data di tbl_import
     * @param type $idImport
     * @param type $jumlahData
     * @return boolean
     */
    private function updateJumlahDataImport($idImport, $jumlahData) {
        $model = ModImport::model()->findByPk($idImport);

        $model->jumlah_data = $jumlahData;

        if ($model->save()) {
            return true;
        } else {
            $this->addErrors($model->errors);
            return false;
        }
    }

    /**
     * Method untuk meload library PHPExcel
     * @return PHPExcel Instance dari class PHPExcel
     */
    private function loadPHPExcel() {

        $phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel.Classes');
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $excel = PHPExcel_IOFactory::load($this->filepath);
        spl_autoload_register(array('YiiBase', 'autoload'));

        return $excel;
    }

    protected function writeLog($activity, $keterangan = null) {

        $id_user = Yii::app()->user->id;
        if (!is_null($keterangan)) {
            $keterangan = CVarDumper::dumpAsString($keterangan);
        }
        ModLog::writeLog($id_user, $activity, $keterangan);
    }

}
