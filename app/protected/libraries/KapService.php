<?php

/**
 * Description of KAPService
 *
 * @author juliardi
 */
class KapService {

    private $serviceUrl;
    private $serviceUsername;
    private $servicePassword;

    public function __construct() {

        $this->serviceUrl = ModSetting::getSettingValue('SERVICE_URL');
        $this->serviceUsername = ModSetting::getSettingValue('SERVICE_USERNAME');
        $this->servicePassword = ModSetting::getSettingValue('SERVICE_PASSWORD');
    }

    /**
     * Method untuk mengambil data dari service dikti berdasar nomor KAP dan PIN 
     * @param type $kap
     * @return jsonObject | null
     */
    public function cekKAP($kap) {
        $httpUrl = new EHttpClient($this->serviceUrl);

        $httpUrl->setParameterGet('kap', $kap);

        $httpUrl->setParameterPost(array(
            'username' => $this->serviceUsername,
            'password' => $this->servicePassword,
        ));


        $hasil = $httpUrl->request('POST');

        return json_decode($hasil->getBody());
    }

    /**
     * Method untuk mendapatkan keterangan status hasil request service
     * @param integer $status Status hasil request
     * @return string Keterangan status <br>
     * 0 : Data tidak ditemukan <br>
     * 1 : Parameter tidak lengkap <br>
     * 2 : Kesalahan username atau password
     */
    public function getErrorString($status) {
        switch ($status) {
            case 1: {
                    return 'Data KAP tidak ditemukan';
                }
            case 2 : {
                    return 'Parameter tidak lengkap';
                }
            case 3 : {
                    return 'Kesalahan username atau password';
                }
        }
    }

}
