<?php

/**
 * Description of ExportData
 *
 * @author juliardi
 */
class ExportData {

    const STARTING_HEADER_ROW_NUM = 1;
    const STARTING_CONTENT_ROW_NUM = 2;

    private $modelClass;
    private $options;
    private $exportedAttributes;
    private $relationAttributes;
    private $phpExcel;
    private $data;

    /**
     * 
     * @param string $modelClass Nama class Model yang ingin diexport
     * @param array $exportedAttributes Array attributes yang ingin diexport
     * @param array $options Array yang digunakan untuk mengambil data. 
     * Parameter ini akan digunakan sebagai parameter untuk memanggil method 
     * CActiveRecord::model()->findAllByAttributes($options)
     */
    public function __construct($modelClass, $options = array()) {
        $this->modelClass = $modelClass;
        $this->options = $options;
        $this->phpExcel = $this->loadPHPExcel();
        $this->loadData();
    }

    public function loadData() {
        $this->data = $this->loadDataFromModel($this->modelClass, $this->options);
    }
    
    /**
     * 
     * @return CActiveRecord[]
     */
    private function loadDataFromModel($modelClass, $options) {
        return CActiveRecord::model($modelClass)->findAllByAttributes($options);
    }

    public function setRelationAttributes($relationAtt) {
        $this->relationAttributes = $relationAtt;
    }

    public function setExportedAttributes($attributes) {
        $this->exportedAttributes = $attributes;
    }

    /**
     * Method untuk mengeksport data dari model
     * @return string Nama file hasil export
     */
    public function exportData() {

        $this->writeData();
        $writer = $this->loadPHPExcelWriter($this->phpExcel);
        $filename = $this->generateFilename();
        $exportPath = Yii::app()->params['exportDir'];

        $writer->save($exportPath . $filename);

        return $filename;
    }

    /**
     * Method untuk meload library PHPExcel
     * @return PHPExcel Instance dari class PHPExcel
     */
    private function loadPHPExcel() {

        $phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel.Classes');
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        include_once ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $phpExcel = new PHPExcel();

        spl_autoload_register(array('YiiBase', 'autoload'));

        return $phpExcel;
    }

    /**
     * Method untuk meload library PHPExcel
     * @return PHPExcel_Writer_Excel2007 Instance dari class PHPExcel
     */
    private function loadPHPExcelWriter($phpExcelInstance) {

        spl_autoload_unregister(array('YiiBase', 'autoload'));

        $phpExcelWriter = new PHPExcel_Writer_Excel2007($phpExcelInstance);

        spl_autoload_register(array('YiiBase', 'autoload'));

        return $phpExcelWriter;
    }

    private function writeData() {
        $this->writeHeaderCell();
        $this->writeContent();
    }

    private function writeHeaderCell() {
        $startColumn = ord("A");
        $startRow = self::STARTING_HEADER_ROW_NUM;

        $model = new $this->modelClass();
        $attributeLabel = $model->attributeLabels();

        foreach ($this->exportedAttributes as $attribute) {
            $cellCoordinate = chr($startColumn++) . $startRow;

            if (array_key_exists($attribute, $attributeLabel)) {
                $this->phpExcel->getActiveSheet()->setCellValue(
                        $cellCoordinate, $attributeLabel[$attribute]
                );
            }
        }

        if (!is_null($this->relationAttributes)) {
            $this->writeRelationHeader($startColumn, $startRow);
        }
    }

    private function writeRelationHeader($startColumn, $startRow) {

        foreach ($this->relationAttributes as $attribute) {
            $cellCoordinate = chr($startColumn++) . $startRow;

            $this->phpExcel->getActiveSheet()->setCellValue(
                    $cellCoordinate, $attribute
            );
        }
    }

    private function writeContent() {
        $startColumn = ord("A");
        $startRow = self::STARTING_CONTENT_ROW_NUM;

        foreach ($this->data as $model) {
            foreach ($this->exportedAttributes as $value) {
                $cellCoordinate = chr($startColumn++) . $startRow;
                $content = $model->$value;

                $this->phpExcel->getActiveSheet()->setCellValue(
                        $cellCoordinate, $content
                );
            }

            if (!is_null($this->relationAttributes)) {
                foreach ($this->relationAttributes as $relation => $attribute) {

                    $cellCoordinate = chr($startColumn++) . $startRow;
                    $content = $model->$relation->$attribute;

                    $this->phpExcel->getActiveSheet()->setCellValue(
                            $cellCoordinate, $content
                    );
                }
            }

            $startColumn = ord("A");
            $startRow++;
        }
    }

    /**
     * Method untuk menggenerate nama file yang diexport
     * @return String Nama file yang disimpan di server
     */
    private function generateFilename() {
        $date = date('His-dmY');
        $userId = Yii::app()->user->id;
        $ext = 'xlsx';

        return 'export-' . $date . '-' . $userId . '.' . $ext;
    }

}
