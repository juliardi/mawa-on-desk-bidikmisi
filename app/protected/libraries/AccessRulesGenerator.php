<?php

/**
 * Description of AccessRulesGenerator
 *
 * @author juliardi
 */
class AccessRulesGenerator {

    public static function getAccessRules($username, $controller) {
        
        $modelUser = ModUser::getModelByUsername($username);
        
        return self::generateRules($modelUser, $controller);
        
    }
    
    private static function generateRules($modelUser, $controller) {
        $rules = array();
        $denyRules = self::getDenyRules();
        
        if(is_null($modelUser)) {
            array_push($rules, $denyRules);
        } else {
            $rules = self::generateCompleteRules($modelUser, $controller);
        }
        
        return $rules;
    }
    
    private static function generateCompleteRules($modelUser, $controller) {
        $completeRules = array();
        $denyRules = self::getDenyRules();
        $allowRules = self::getAllowRules($modelUser->getRoleName(), $controller);
        
        if (!is_null($allowRules)) {
            array_push($completeRules, $allowRules);
        }
        array_push($completeRules, $denyRules);

        return $completeRules;
    }
    
    private static function getDenyRules() {
        return array(
            'deny',
            'users' => array('*'),
        );
    }

    private static function getAllowRules($roleName, $controller) {

        $id_role = ModRole::getIdRoleByName($roleName);
        $accessList = ModAccessRules::getAccessRules($id_role, $controller);

        if (count($accessList) == 0) {
            return null;
        } else {
            return self::generateAllowRules($roleName, $accessList);
        }
    }

    private static function generateAllowRules($roleName, $accessList) {
        $arrAllow = array('allow');
        $actions = array();

        foreach ($accessList as $model) {
            array_push($actions, $model->action);
        }

        $arrAllow['actions'] = $actions;
        $arrAllow['expression'] = 'Yii::app()->user->hasRole("' . $roleName . '")';

        return $arrAllow;
    }

}
