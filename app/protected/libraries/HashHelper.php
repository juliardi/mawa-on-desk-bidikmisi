<?php

/**
 * Description of HashHelper
 *
 * @author juliardi
 */
class HashHelper {
    
    /**
     * Hash data dan salt dengan algoritma SHA-256
     * @param type $data
     * @param type $salt
     * @return type
     */
    public static function hash($data, $salt) {
        return hash('sha256', $data+$salt);
    }
    
    /**
     * Menggenerate salt dengan CSecurityManager Yii
     * @return type
     */
    public static function generateSalt() {
        $security = new CSecurityManager();
        
        return $security->generateRandomString(128, true);
    }
    
}
