<?php

Yii::setPathOfAlias('booster', dirname(__FILE__) . DIRECTORY_SEPARATOR . '../extensions/yiibooster');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Aplikasi Bidik Misi',
    'timeZone' => 'Asia/Jakarta',
    // preloading 'log' component
    'preload' => array('log', 'bootstrap'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.models.extended.*',
        'application.components.*',
        'application.extensions.EHttpClient.*',
        'application.extensions.yiibooster.widgets.*',
        'application.libraries.*',
        'application.forms.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'password',
            'generatorPaths' => array(
                'booster.gii',
            ),
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('*', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'simplesamlphp' => array(
            'class' => 'ext.yii-simplesamlphp.components.Simplesamlphp',
            'autoloadPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR .
                              '../../../simplesamlphp-sp/lib/_autoload.php',
            'authSource' => 'default-sp',
        ),
        'user' => array(
           'class' => 'ext.yii-simplesamlphp.components.SSOWebUser',
//             'class' => 'application.components.WebUser',
        ),
        'bootstrap' => array(
            'class' => 'booster.components.Booster',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'juliardi@student.uns.ac.id',
        'uploadDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../uploads/',
        'exportDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../exports/',
        'imagesDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../images/',
    ),
);
