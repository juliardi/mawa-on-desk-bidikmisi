<?php

class FiturFilter extends CFilter {

    public $allowedFiturs;

    protected function preFilter($filterChain) {
        $hasAccess = false;
        foreach ($this->allowedFiturs as $fitur) {
            $hasAccess = $hasAccess || Yii::app()->user->hasFitur($fitur);
        }

        if (!$hasAccess) {
            Yii::app()->user->setFlash('error', 'Anda tidak diperbolehkan mengakses fitur ini.');
            throw new CHttpException(403, 'Anda tidak diperbolehkan mengakses fitur ini.');
        } else {
            return true;
        }
    }

}
