<?php

class RoleFilter extends CFilter {

    public $allowedRoles;

    protected function preFilter($filterChain) {
        $hasAccess = false;
        foreach ($this->allowedRoles as $role) {
            $hasAccess = $hasAccess || Yii::app()->user->hasRole($role);
        }

        if (!$hasAccess) {
            Yii::app()->user->setFlash('error', 'Anda tidak diperbolehkan mengakses fitur ini.');
            throw new CHttpException(403, 'Anda tidak diperbolehkan mengakses fitur ini.');
        } else {
            return true;
        }
    }

}
