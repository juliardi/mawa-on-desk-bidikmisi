<?php

class SSOWebUser extends CWebUser {

        /**
     * The component name which is you register Simplesamlphp instance in your config/main.php.
     */
    public $simplesamlphpComponentName = 'simplesamlphp';
    /**
     * Simplesamlphp component instance.
     */
    private $simplesamlphpInstance = null;
    /**
     * Init the SSOWebUser.
     */
    public function init() {
        assert(!is_null($this->simplesamlphpComponentName), 'You must set simplesamlphp component name.');
        $componentName = $this->simplesamlphpComponentName;
        $this->simplesamlphpInstance = Yii::app()->$componentName;
        parent::init();
    }

    private function getSimplesamlphpInstance() {
        $temp = $this->simplesamlphpComponentName;
        // var_dump($temp);
        // Yii::app()->end();
        return Yii::app()->$temp;
    }

    public function getName() {
        $data = json_decode($this->getSimplesamlphpInstance()->data);
        if ($data == NULL) {
            return Null;
        } else {
            return $data->email;
        }
    }

    public function getId() {

        if ($this->isGuest()) {
            return null;
        }

        $user = ModUser::getModelByUsername($this->getName());

        if (is_null($user)) {
            return null;
        }

        return $user->id_user;
    }

    public function hasRole($role) {

        if ($this->isGuest()) {
            return false;
        }

        $user = ModUser::getModelByUsername($this->getName());

        if (is_null($user)) {
            return false;
        }

        return ($user->getRoleName() === $role);
    }

    public function getRoleName() {

        if ($this->isGuest()) {
            return '';
        }

        $user = ModUser::getModelByUsername($this->getName());

        if (is_null($user)) {
            return null;
        }

        return $user->getRoleName();
    }
    
    public function isGuest() {
        return !$this->getSimplesamlphpInstance()->isAuthenticated();
    }

}
