<?php

/**
 * Description of UserMenuBar
 *
 * @author asasmoyo
 */
class UserMenuBar extends CWidget {

    public $type = 'navbar';
    public $loginRoute;
    public $logoutRoute;
    public $nama;
    public $username;
    public $email;
    public $profilUrl = 'http://profil.uns.ac.id';
    public $simplesamlphpComponentName = 'simplesamlphp';

    public function run() {
        $html = '<ul class="nav navbar-nav navbar-right">';
        if (Yii::app()->user->isGuest) {
            $html .= $this->getLoginMenu();
        } else {
            $html .= $this->getUserMenu();
        }
        $html .= '</ul>';
        echo $html;
    }

    private function getLoginMenu() {
        return '<li><a href="' . Yii::app()->createUrl($this->loginRoute) . '"><span class="glyphicon glyphicon-user"></span> Login</a></li>';
    }

    private function getUserMenu() {
        $html = '<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span>
                        <strong>' . $this->nama . '</strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span class="glyphicon glyphicon-user icon-size"></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong>' . $this->nama . '</strong></p>
                                        <p class="text-left small">' . $this->email . '</p>
                                        <p class="text-left">
                                            <a href="' . $this->profilUrl . '" class="btn btn-primary btn-block btn-sm">Ganti Profil</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="' . Yii::app()->createUrl($this->logoutRoute) . '" class="btn btn-danger btn-block">Logout</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>';
        return $html;
    }

}
