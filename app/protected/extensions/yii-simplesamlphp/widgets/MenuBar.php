<?php

/**
 * Description of UserMenuBar
 *
 * @author dipta
 */
class MenuBar extends CWidget {

    public $type = 'navbar';

    public function run() {
        $html = '<ul class="nav navbar-nav">';
        if (Yii::app()->user->isGuest) {
            $html .= $this->getDefaultMenu();
        } else {
            $html .= $this->getCustomMenu();
        }
        $html .= '</ul>';
        echo $html;
    }

    private function getDefaultMenu() {
        return '<li><a href="' . Yii::app()->createUrl('site/index') . '">Home</a></li>'
                . '<li><a href="' . Yii::app()->createUrl('site/lihatberita&id=1') . '">Berita terbaru</a></li>';
    }

    private function getCustomMenu() {
        $level= Yii::app()->user->level;
        
        $datamenu=  Yii::app()->db->createCommand('select id_menu,nama_menu from menu_user_umum where parent is null and id_aktor='.$level)
                ->queryAll();
      //  var_dump($datamenu);
       // Yii::app()->end();
        
        $html='';
        foreach ($datamenu as $menuutama){
        $html .= '
                <li class="dropdown">
                    <a href="site" class="dropdown-toggle" data-toggle="dropdown">'.$menuutama['nama_menu'].'
                    <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        '.$this->getSubMenu($menuutama['id_menu']).'
                    </ul>
                 </li>   ';
        }
        return $html;
    }
    
    private function getSubMenu($id) {
        $html='';
        $level= Yii::app()->user->level;
        $datasubmenu=  Yii::app()->db->createCommand('Select m.nama_menu, f.url from menu_user_umum m, hak_akses_umum h, fitur f '
                . 'where m.id_akses=h.id_akses and h.id_fitur=f.id_fitur and h.status=true and h.id_aktor=' . $level . 'and m.id_aktor='
                .  $level . ' and parent='.$id)
                ->queryAll();
        foreach ($datasubmenu as $submenu){
            $html.='<li><a href="'. Yii::app()->createUrl($submenu['url']).'">'.$submenu['nama_menu'].'</a></li>';
        }
        return $html;
    }

}

?>
