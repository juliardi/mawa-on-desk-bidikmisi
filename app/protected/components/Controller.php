<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//theme/adminLayout';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    
    public function filters() {
        return array(
            'accessControl',
        );
    }

    protected function beforeAction($action) {
        
        $menu = $this->generateMenu();
        $userRole = Yii::app()->user->getRoleName();
        
        if(Yii::app()->user->isGuest) {
            $this->menu = $menu['default'];
        } else {
            $this->menu = $menu[$userRole];
        }

        return parent::beforeAction($action);
    }
    
    private function generateMenu() {
        $menuAdmin = array(
            array(
                'label' => 'Logout',
                'url' => Yii::app()->createUrl('site/logout'),
                'icon' => 'fa fa-user fa-fw',
            ),
            array(
                'label' => 'Manajemen Export-Import',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'Import Data Siswa',
                'url' => Yii::app()->createUrl('import/listImport'),
                'icon' => 'glyphicon glyphicon-import',
            ),
            array(
                'label' => 'Export Data Siswa',
                'url' => Yii::app()->createUrl('export/exportSiswa'),
                'icon' => 'glyphicon glyphicon-export',
            ),
            array(
                'label' => 'Manajemen Prodi',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Prodi',
                'url' => Yii::app()->createUrl('prodi/listProdi'),
                'icon' => 'glyphicon glyphicon-list',
            ),
            array(
                'label' => 'Manajemen User',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List User',
                'url' => Yii::app()->createUrl('user/listUser'),
                'icon' => 'fa fa-users fa-fw',
            ),
            array(
                'label' => 'Manajemen Observasi',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Observasi',
                'url' => Yii::app()->createUrl('observasi/listObservasi'),
                'icon' => 'glyphicon glyphicon-list-alt',
            ),
            array(
                'label' => 'Manajemen Tahun Aktif',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'Pilih Tahun Aktif',
                'url' => Yii::app()->createUrl('tahunAktif/pilihTahun'),
                'icon' => 'glyphicon glyphicon-saved',
            ),
            array(
                'label' => 'List Tahun',
                'url' => Yii::app()->createUrl('tahunAktif/listTahun'),
                'icon' => 'glyphicon glyphicon-list',
            ),
            array(
                'label' => 'Manajemen Persyaratan',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Persyaratan',
                'url' => Yii::app()->createUrl('syarat/listSyarat'),
                'icon' => 'glyphicon glyphicon-list',
            ),
            
        );
        
        $menuOperator = array(
            array(
                'label' => 'Logout',
                'url' => Yii::app()->createUrl('site/logout'),
                'icon' => 'fa fa-user fa-fw',
            ),
            array(
                'label' => 'Manajemen Siswa',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Siswa',
                'url' => Yii::app()->createUrl('siswa/listSiswa'),
                'icon' => 'fa fa-users fa-fw',
            ),
        );
        
        $menuPetugas = array(
            array(
                'label' => 'Logout',
                'url' => Yii::app()->createUrl('site/logout'),
                'icon' => 'fa fa-user fa-fw',
            ),
            array(
                'label' => 'Manajemen Observasi',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Observasi',
                'url' => Yii::app()->createUrl('observasi/listObservasi'),
                'icon' => 'glyphicon glyphicon-list-alt',
            ),
        );
        
        $menuSuperadmin = array(
            array(
                'label' => 'Logout',
                'url' => Yii::app()->createUrl('site/logout'),
                'icon' => 'fa fa-user fa-fw',
            ),
            array(
                'label' => 'Manajemen Rule',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Rule',
                'url' => Yii::app()->createUrl('rules/listRules'),
                'icon' => 'glyphicon glyphicon-list',
            ),
            array(
                'label' => 'Setting',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Setting',
                'url' => Yii::app()->createUrl('setting/listSetting'),
                'icon' => 'glyphicon glyphicon-wrench',
            ),
            array(
                'label' => 'Log',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Log',
                'url' => Yii::app()->createUrl('log/listLog'),
                'icon' => 'glyphicon glyphicon-list-alt',
            ),
        );
        
        $menuPimpinan = array(
            array(
                'label' => 'Logout',
                'url' => Yii::app()->createUrl('site/logout'),
                'icon' => 'fa fa-user fa-fw',
            ),
            array(
                'label' => 'Laporan',
                'itemOptions' => array(
                    'style' => 'background-color: #3071A9; padding: 10px; color:#DDD;'
                ),
            ),
            array(
                'label' => 'List Siswa',
                'url' => Yii::app()->createUrl('laporan/listSiswa'),
                'icon' => 'fa fa-users fa-fw',
            ),
            array(
                'label' => 'List Observasi',
                'url' => Yii::app()->createUrl('laporan/listObservasi'),
                'icon' => 'glyphicon glyphicon-list-alt',
            ),
            array(
                'label' => 'Rekap Data',
                'url' => Yii::app()->createUrl('laporan/rekapData'),
                'icon' => 'glyphicon glyphicon-list',
            ),
        );
        
        $menuDefault = array(
            array(
                'label' => 'Login',
                'url' => Yii::app()->createUrl('site/login'),
                'icon' => 'fa fa-user fa-fw',
            ),
        );
        
        $menu['admin'] = $menuAdmin;
        $menu['operator'] = $menuOperator;
        $menu['petugas'] = $menuPetugas;
        $menu['default'] = $menuDefault;
        $menu['superadmin'] = $menuSuperadmin;
        $menu['pimpinan'] = $menuPimpinan;
        
        return $menu;
    }
    
    protected function writeLog($activity, $keterangan = null) {
        
        $id_user = Yii::app()->user->id;
        if(!is_null($keterangan)) {
            $keterangan = CVarDumper::dumpAsString($keterangan);
        }
        ModLog::writeLog($id_user, $activity, $keterangan);
        
    }
    
    protected function sendSuccessJsonResponse() {
        $this->sendJsonResponse(array(
            'result' => 'success',
        ));
    }
    
    protected function sendErrorJsonResponse() {
        $this->sendJsonResponse(array(
            'result' => 'error',
        ));
    }
    
    protected function sendJsonResponse($array) {
        echo json_encode($array);
    }

}
