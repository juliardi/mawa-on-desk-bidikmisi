<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebUser
 *
 * @author juliardi
 */
class WebUser extends CWebUser {
    
    public function getId() {
        
        if(Yii::app()->user->isGuest) {
            return null;
        }
        
        $user = ModUser::getModelByUsername($this->getName());
        
        return $user->id_user;
    }
    
    public function hasRole($role) {
        
        if(Yii::app()->user->isGuest) {
            return false;
        }
        
        $user = ModUser::getModelByUsername($this->getName());
        
        return ($user->getRoleName() === $role);
    }
    
    public function getRoleName() {
        
        if(Yii::app()->user->isGuest) {
            return '';
        }
        
        $user = ModUser::getModelByUsername($this->getName());
        
        return $user->getRoleName();
        
    }
        
}
