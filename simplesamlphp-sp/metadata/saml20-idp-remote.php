<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */
$metadata['http://login.uns.ac.id/saml2/idp/metadata.php'] = array (
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'http://login.uns.ac.id/saml2/idp/metadata.php',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://login.uns.ac.id/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://login.uns.ac.id/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'certData' => 'MIIEITCCAwmgAwIBAgIJAMZaldx39kIPMA0GCSqGSIb3DQEBCwUAMIGmMQswCQYDVQQGEwJJRDEUMBIGA1UECAwLSmF3YSBUZW5nYWgxEjAQBgNVBAcMCVN1cmFrYXJ0YTEiMCAGA1UECgwZVW5pdmVyc2l0YXMgU2ViZWxhcyBNYXJldDEUMBIGA1UECwwLVVBULiBQdXNrb20xEjAQBgNVBAMMCXVucy5hYy5pZDEfMB0GCSqGSIb3DQEJARYQcHVza29tQHVucy5hYy5pZDAeFw0xNTAzMTgwNzQ1NDlaFw0yNTAzMTcwNzQ1NDlaMIGmMQswCQYDVQQGEwJJRDEUMBIGA1UECAwLSmF3YSBUZW5nYWgxEjAQBgNVBAcMCVN1cmFrYXJ0YTEiMCAGA1UECgwZVW5pdmVyc2l0YXMgU2ViZWxhcyBNYXJldDEUMBIGA1UECwwLVVBULiBQdXNrb20xEjAQBgNVBAMMCXVucy5hYy5pZDEfMB0GCSqGSIb3DQEJARYQcHVza29tQHVucy5hYy5pZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMZHMHYhdAjae1bk8r0/q+68TZssudV6t7vOFKrG125vYNddyBbwmEBtHFtgLGkHPMl/mwI51u0kv8yUulHgCX/qcTHHtuaJTzQfCgJXfYtGnPP5KweQhZPIRGvmWEf4ujhG9AYD6eoHjeLxUfFMXPj8T0xziuHKsgkW3INSPr3TwPo7r5Oom3T+3psNyDGoU86+CBFXuNAYUBECLk57VgZa0+hIgxaKuBXRK15X8i3D38BuZPjJ+ffPnpsy7UIj/11Rbvvc+Ky+zWugXvZBukYS6XP+Oy2LURkGAi80xNAwCRscnXMcFYLxE1vHIR7BuR70BSpm53I0BP0IGRlFcecCAwEAAaNQME4wHQYDVR0OBBYEFKGArDumhMRaz091buLFkKNpsqaqMB8GA1UdIwQYMBaAFKGArDumhMRaz091buLFkKNpsqaqMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAE5OCLJJ9bDQIaUN5JMRGM2tdy9YYv2HPpr7zavMVFLAIeOar7GnDGmLAGQf+xfWNdVHhMw2YReeV8H6p+b0plMr0YcPf7D9G8bKqB7flzPOOrU1w2eHmEDv9FDMumkwb2Z9WwD+u3MWhDYB7GvSgLQWqePAPwAWHYbESIH7tMWUWs+Y2z0ZxmHaeSB+lCcXbmAa0y2OSHsGGXNy9IQcCZ9eiYxYXtmAeDaILZkB/dGCpk6FeoAIOFjQBMhYTbOV86qIuZ3hTJE8Q0B7YvSRvS0cQa7QVFq2z6FP1jm8RmHAj7OEhMv7dWTAuOk86X9IPV4ca7Zw+XWEKZZOO5m+eoE=',
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
);
